<?php
require_once("./include_company/membersite_config.php");

if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("login_company.php");
    exit;
}
$ceo=$_POST['ceo'];
$about=$_POST['about'];
$address=$_POST['address'];
$contact_details=$_POST['contact_details'];
$date=$_POST['date'];
$founders=$_POST['founders'];

 $fgmembersite->save_ceo($ceo,$fgmembersite->UserEmail()); 
 $fgmembersite->save_about($about,$fgmembersite->UserEmail()); 
 $fgmembersite->save_address($address,$fgmembersite->UserEmail()); 
 $fgmembersite->save_contact_details($contact_details,$fgmembersite->UserEmail());
 $fgmembersite->save_date($date,$fgmembersite->UserEmail());
 $fgmembersite->save_founder($founders,$fgmembersite->UserEmail());
 $fgmembersite->RedirectToURL("update_company.php");
?>