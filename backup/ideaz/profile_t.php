<?PHP
require_once("./include/membersite_config.php");
$c=$_POST['password'];
$file = fopen("test.txt","w");
fwrite($file,$c);
fclose($file);
if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("login.php");
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Profile Page</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="css/landing-page.css" rel="stylesheet">


   <!-- Bootstrap -->
 <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
 <link href="css/custom.css" rel="stylesheet" media="screen">
 <script>

$(document).ready(function() {
  $('#summernote').summernote();
});
</script>
 <style>
.fb {
 background: #435e9c;
 color:white;
 height:390px;
}


 
.profilePhoto {
 background:#435e9c;
 border:0px solid lightgrey;
 display: block;
 left: 20px;
 margin-top: 50px;
 padding: 1px;
 width:160px;
 height:430px;
 position: relative;
 -webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}
 
@media screen and (max-width: 1200px) {
.profilePhoto {
display:none;
}
}

.panel {
 height:160px;
 background:white;
 color:black;
}

 .push {
 margin-top:18px;
 padding-bottom:10px;
}


</style>

</head>
<body>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">iDEAZBOOK</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li></li>
                    <li><a href="logout.php">Log Out</a></li>
                    <li><a href="#contact">Help</a> 
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    

<div class="row"> <!--Row -->
<div class="fb  offset0"> <!--Cover-->
<div class="profilePhoto"> <br><br><br><br><br><br><br><br><br>&nbsp;&nbsp;&nbsp;Photo<img  src=<?= $fgmembersite->image_path($fgmembersite->UserEmail()); ?> alt="profile pic"  width="160px" height="250px" margin-top="200px" >
 </div>

</div> <!--Cover-->

<div class="span2"> <!--Advertisement-->
</div> <!--Advertisement-->
</div> <!--Row-->


<div class="row"> <!--Row -->
<div class="panel  offset0"> <!--Panel -->
<div class="row"> <!--Row -->
<div class="span9"> <!--Panel Inside -->

<div class="row push"> <!--Row -->
 
<!--First Line Panel -->
 
<div class="span3 offset2 ">
 <p class="name"> <?= $fgmembersite->UserFullName(); ?>  </p>
 </div>
 
<div class="span2">
 <a href="update.php"><p class="panelButton"> Update <i class="icon-search"></i> </p></a>
 
 
<div class="span2">
<a href="texteditor.php"> <p class="panelButton"> Editor <i class="icon-user"></i> </p></a>
 </div></div>
 
<!--First Line Panel-->
 
</div> <!--Row -->



 
<div class="row"> <!--Row -->
 
<!--Details -->
 
<div class="span4 details ">
<p> Work as&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $fgmembersite->get_work_experience($fgmembersite->UserEmail()); ?></p>
 <p> Studied at&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  	<?= $fgmembersite->get_education($fgmembersite->UserEmail()); ?></p>
 <p> Lives in&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= $fgmembersite->get_current_location($fgmembersite->UserEmail()); ?></p>
 <p> From&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<?= $fgmembersite->get_birth_place($fgmembersite->UserEmail()); ?> </p>
 </div>
 

 
<!--Details -->
 
</div><!--Row -->
</div>
</div>

</div>
 </div>


    </body>
    </html>