<?PHP
require_once("./include/membersite_config.php");
$c=$_POST['password'];

if(isset($_POST['submitted']))
{ 
   if($fgmembersite->RegisterUser())
   {
        $fgmembersite->RedirectToURL("thank-you.html");
   } 
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sign Up</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="css/landing-page.css" rel="stylesheet">

</head>
<body>
<?php include './htmlMod/nav.php';?>
<!--------------------------------------------------------Sign In--------------------------------------------------------------------->

<div  class=" modal1 show" tabindex="-1" role="dialog" aria-hidden="true">
<!--<div class="modal-dialog" style="margin-left:35px"  >
<div class="" >
      <div class="">
<img src="img/sign.jpg" class="img-rounded" height="522px" width="695px">
</div></div></div>-->

<div  class=" modal1 show" tabindex="-1" role="dialog" aria-hidden="true">
<!--  <div class="modal-dialog" style="margin-right:35px">-->
<div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
         <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
          <h1 class="text-center">Sign Up</h1>
      </div>
      <div class="modal-body">
        <form id='register' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8' >
  <input type='hidden' name='submitted' id='submitted' value='1'/>  
<span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span>
            <div class="form-group form-inline"><label class="control-label">Full Name*</label>
              <input type="text" class="form-control input-lg" placeholder="Your Full Name"  name='name' id='name' value='<?php echo $fgmembersite->SafeDisplay('name') ?>' maxlength="50">
  <span id='register_name_errorloc' class='error' ></span>
            </div>
            <div class="form-group form-inline"><label class="control-label">Email* </label>
              <input type="text" class="form-control input-lg" placeholder="Email (name@example.com)"  name='email' id='email' value='<?php echo $fgmembersite->SafeDisplay('email') ?>' maxlength="50" width="200px">
            </div>
<div class="form-group form-inline" > <label class="control-label">Username*</label>
              <input type="text" class="form-control input-lg" placeholder="Username" name='username' id='username' value='<?php echo $fgmembersite->SafeDisplay('username') ?>' maxlength="50" >
            </div>
<div class="form-group form-inline"><label class="control-label">Password*</label>
              <input type="password" class="form-control input-lg" placeholder="Password" name='password' id='password' value='<?php echo $fgmembersite->SafeDisplay('password') ?>' maxlength="50">
            </div>

                        <div class="form-group">
              <button class="btn btn-primary btn-lg btn-block">Sign Up</button>
              
            </div>
          </form>
      </div>
      <div class="modal-footer"> iDEAZBOOK<span class="pull-left"><a href="login.php">Log In</a></span>
          <!--<div class="col-md-12">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
		  </div>-->	
      </div>
  </div>
  </div>
</div>
<!----------------------------------------------------------------------------------------------------------------------------->
 <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>

</body>

</html>