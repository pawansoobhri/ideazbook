<?PHP
require_once("./include/membersite_config.php");
$c=$_POST['password'];



if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("login.php");
    exit;
}
if(isset($_POST['submitted']))
{
   if($fgmembersite->ChangePassword())
   {
        $fgmembersite->RedirectToURL("update.php");
   }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Update</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="css/landing-page.css" rel="stylesheet">


</head>
<body>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="profile.php">iDEAZBOOK</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li></li>
                    <li><a href="logout.php">Log Out</a></li>
                    <li><a href="#contact">Help</a> 
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
<br>
<br>
<hr>

<p><B>EDIT PROFILE</b></P>
    
  	<hr>
	<div class="row">
      <!-- left column -->
      <div class="col-md-3">
        <div class="text-center">







<form action="upload_save.php" method="post"
enctype="multipart/form-data">
          
       <img  src=<?= $fgmembersite->image_path($fgmembersite->UserEmail()); ?> alt="profile pic"  width="160px" height="230px" style="margin-top:100px; display:block">


          <h6>Upload a different photo...</h6>
          
          <input class="form-control" type="file" onchange="this.form.submit()" name="file" >
          <noscript><input type="submit" name="submit" value="Profile Picture"></noscript>
        </div>
      </div>
      </form>
      <!-- edit form column -->
      <div class="col-md-9 personal-info">
       
        <h3>General info</h3>
        
       <form id='changepwd' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
   <div class="form-group">
<input type='hidden' name='submitted' id='submitted' value='1'/>
             <span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span>
            <label class="col-lg-3 control-label">Full Name:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->UserFullName(); ?>" name="nam" readonly>
            </div>
          </div>
          
          
          <div class="form-group">
            <label class="col-lg-3 control-label">Email:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->UserEmail(); ?>" readonly>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-md-3 control-label">Username:</label>
            <div class="col-md-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->UserName($fgmembersite->UserEmail()); ?>" readonly>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Original Password:</label>
            <div class="col-md-8">
              <input class="form-control" type="password" value="" name='oldpwd'>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">New password:</label>
            <div class="col-md-8">
              <input class="form-control" type="password" value="" name='newpwd'>
            </div>
          </div>
            <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input class="btn btn-primary" type="submit" value="Save Changes" name='Submit'>
              <span></span>
              <input class="btn btn-default" type="reset" value="Cancel" >
            </div>
          </div>
          <hr>
          </form>
        <h3>Personal info</h3>
        
        
        <form action="save_info.php" method="post"
enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-lg-3 control-label">Birth Date:</label>
            <div class="col-lg-8">
              <input class="form-control" type="date" value="<?= $fgmembersite->get_date($fgmembersite->UserEmail()); ?>" name="date">
            </div>
          </div>
          
          
          <div class="form-group">
            <label class="col-lg-3 control-label">Birth Place:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->get_birth_place($fgmembersite->UserEmail()); ?>" name="birth_place">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Current Location:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->get_current_location($fgmembersite->UserEmail()); ?>" name="current_location">
            </div>
          </div>
          
     
          <div class="form-group">
            <label class="col-md-3 control-label">Education:</label>
            <div class="col-md-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->get_education($fgmembersite->UserEmail()); ?>" name="education">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Work Experience(if any):</label>
            <div class="col-md-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->get_work_experience($fgmembersite->UserEmail()); ?>" name="work_experience">
            </div>
          </div>
        
        <div class="form-group">
            <label class="col-lg-3 control-label">Current job:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->get_current_job($fgmembersite->UserEmail()); ?>" name="current_job">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input class="btn btn-primary" type="submit" value="Save Changes" >
              <span></span>
              <input class="btn btn-default" type="reset" value="Cancel">
            </div>
          </div>
        </form>
      </div>
  </div>

<hr>
</body>
</html>