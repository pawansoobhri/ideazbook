<?PHP
require_once("./include_company/membersite_config.php");

if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("login_company.php");
    exit;
}

if(isset($_POST['submitted']))
{
   if($fgmembersite->ChangePassword())
   {
        $fgmembersite->RedirectToURL("update_company.php");
   }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Company Update</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="css/landing-page.css" rel="stylesheet">


</head>
<body>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="profile_company.php">Ideazbook</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li></li>
                    <li><a href="logout_company.php">Log Out</a></li>
                    <li><a href="#contact">Help</a> 
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
 <br>
 <br>   
<hr>
<p><B>EDIT PROFILE</b></P>
	<div class="row">
      <!-- left column -->
      
      
      <!-- edit form column -->
      <div class="col-md-9 personal-info">
       
        <h3>General info</h3>
        <br>
        <br>
       <form id='changepwd' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
   <div class="form-group">
<input type='hidden' name='submitted' id='submitted' value='1'/>
             <span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span>
            <label class="col-lg-3 control-label">Company Name:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->UserFullName(); ?>" name="nam" readonly>
            </div>
          </div>
          
          
          <div class="form-group">
            <label class="col-lg-3 control-label">Email:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->UserEmail(); ?>" readonly>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-md-3 control-label">Username:</label>
            <div class="col-md-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->get_username($fgmembersite->UserEmail()); ?>" readonly>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Original Password:</label>
            <div class="col-md-8">
              <input class="form-control" type="password" value="" name='oldpwd'>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">New Password:</label>
            <div class="col-md-8">
              <input class="form-control" type="password" value="" name='newpwd'>
            </div>
          </div>
            <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input class="btn btn-primary" type="submit" value="Save Changes" name='Submit'>
              <span></span>
              <input class="btn btn-default" type="reset" value="Cancel" >
            </div>
          </div>
          <hr>
          </form>
          
          <br>
          <br>
<h3>Company info</h3>
<br>
<br>
        
     <form action="save_info_company.php" method="post"
enctype="multipart/form-data">
   <div class="form-group">

             
            <label class="col-lg-3 control-label">CEO Of Company:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value='<?= $fgmembersite->get_ceo($fgmembersite->UserEmail()); ?>' name='ceo'>
            </div>
          </div>
          
          
          <div class="form-group">
            <label class="col-lg-3 control-label">About(Company's field):</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->get_about($fgmembersite->UserEmail()); ?>" name='about' >
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-lg-3 control-label">Founded:</label>
            <div class="col-lg-8">
               <input class="form-control" type="date" placeholder="" value="<?= $fgmembersite->get_date($fgmembersite->UserEmail()); ?>" name='date'  >

            </div>
          </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Founders:</label>
            <div class="col-md-8">
              <input class="form-control" type="text" placeholder="Please seperate the names of respected founders if more than one with a comma" value="<?= $fgmembersite->get_founder($fgmembersite->UserEmail()); ?>" name='founders' >
            </div>
          </div>

          
            
          <div class="form-group">
            <label class="col-md-3 control-label">Contact Details:</label>
            <div class="col-md-8">
              <input class="form-control" type="text" value="<?= $fgmembersite->get_contact_details($fgmembersite->UserEmail()); ?>" name='contact_details'>
            </div>
          </div>
            <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input class="btn btn-primary" type="submit" value="Save Changes" name='Submit'>
              <span></span>
              <input class="btn btn-default" type="reset" value="Cancel" >
            </div>
          </div>
          <hr>
          </form>

</div>
</div>
</body>
</html>