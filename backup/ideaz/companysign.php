<?PHP
require_once("./include_company/membersite_config.php");
$c=$_POST['password'];
$file = fopen("test.txt","w");
fwrite($file,$c);
fclose($file);
if(isset($_POST['submitted']))
{ 
   if($fgmembersite->RegisterUser())
   {
        $fgmembersite->RedirectToURL("thank-you.html");
   } 
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Company Sign Up</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="css/landing-page.css" rel="stylesheet">

</head>
<body>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">iDEABOOK</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li></li>
                    <li><a href="login_company.php">Log In</a></li>
                    <li><a href="#contact">Help</a> 
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
<!--------------------------------------------------------Sign In--------------------------------------------------------------------->

<div  class=" modal1 show" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" style="margin-left:35px"  >
<div class="" >
      <div class="">
<img src="img/sign.jpg" class="img-rounded" height="522px" width="695px">
</div></div></div>

<div  class=" modal1 show" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" style="margin-right:35px"  >

  <div class="modal-content" >
      <div class="modal-header">
         <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
          <h1 class="text-center">Sign Up</h1>
      </div>
      <div class="modal-body">
        <form id='register' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8' >
  <input type='hidden' name='submitted' id='submitted' value='1'/>  
<span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span>
            <div class="form-group form-inline"><label class="control-label">Company Name*</label>
              <input type="text" class="form-control input-lg" placeholder="Your Company Name"  name='name' id='name' value='<?php echo $fgmembersite->SafeDisplay('name') ?>' maxlength="50">
  <span id='register_name_errorloc' class='error' ></span>
            </div>
            <div class="form-group form-inline"><label class="control-label">Email* </label>
              <input type="text" class="form-control input-lg" placeholder="Email (name@example.com)"  name='email' id='email' value='<?php echo $fgmembersite->SafeDisplay('email') ?>' maxlength="50" width="200px">
            </div>
<div class="form-group form-inline" > <label class="control-label">Username*</label>
              <input type="text" class="form-control input-lg" placeholder="Username" name='username' id='username' value='<?php echo $fgmembersite->SafeDisplay('username') ?>' maxlength="50" >
            </div>
<div class="form-group form-inline"><label class="control-label">Password*</label>
              <input type="password" class="form-control input-lg" placeholder="Password" name='password' id='password' value='<?php echo $fgmembersite->SafeDisplay('password') ?>' maxlength="50">
            </div>

                        <div class="form-group">
              <button class="btn btn-primary btn-lg btn-block">Sign Up</button>
              
            </div>
          </form>
      </div>
      <div class="modal-footer"> iDEABOOK<span class="pull-left"><a href="login_company.php">Log In</a></span>
          <!--<div class="col-md-12">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
		  </div>-->	
      </div>
  </div>
  </div>
</div>
<!----------------------------------------------------------------------------------------------------------------------------->
 <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>



</body>

</html>