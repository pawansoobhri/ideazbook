<?PHP
require_once("./include/membersite_config.php");
if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("login.php");
   exit;
   }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
    <title> <?= $fgmembersite->UserFullName(); ?></title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="assets/js/hover.zoom.js"></script>
    <script src="assets/js/hover.zoom.conf.js"></script>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  </head>

  <body> 
<?php include './htmlMod/profileNav.php';?>
        <!-- /.container -->
        <div class="navbar navbar-inverse navbar-static-top" style="padding-bottom:0px; background-color:white">
      <div class="container">
        <div class="navbar-header">
       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        
<a class="navbar-brand"><?= $fgmembersite->UserFullName(); ?></a>
        
<!-- <div id="custom-search-input"><div class="input-group col-xs-2 col-sm-2"><input type="text" class=" search-query form-control" placeholder="Search" \> 
<span class="input-group-btn">
<button class="btn" type="submit">
<i class="icon-search">
</i>
</button>
</span>	</div>	
</div>--></div>
    </div></div>	
<div class="navbar navbar-inverse navbar-static-top" style="padding-bottom:2%;padding-top:2%">
      <div class="container">

<div class="col-xs-2 col-sm-1.5"></div> <div class="col-xs-4 col-sm-3">
                                              
                            <p></p>
                          <a href="" >  <button class="btn btn-info btn-block"><span class="fa fa-user"></span> Home </button></a>
                        </div><!--/col-->
                        <div class="col-xs-4 col-sm-3">
                                               
                            <p></p>
                         <a href="update.php">   <button class="btn btn-info btn-block"><span class="fa fa-user"></span> Update Profile </button></a>
                        </div><!--/col-->
                        <div class="col-xs-4 col-sm-3">
                                              
                            <p></p>
                          <a href="texteditor.php">   <button class="btn btn-info btn-block" type="button"><span class="fa fa-user"></span> Editor</button> </a> 
                        </div>
      </div>
    </div>
      <div class="container">
		 <div class="row">
		 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-3 col-sm-offset-3 col-md-offset-0 col-lg-offset-0 toppad">
<div class="panel panel-info">
<div class="panel-body">
<div class="row">
<div class="col-md-5 col-lg-5"><img  src=<?= $fgmembersite->image_path($fgmembersite->UserEmail()) ;?> alt="profile pic"  width="300px" height="280px" margin-top="200px"  ></div>
<div class="col-md-6 col-lg-6">
<table class="table table-user-information">
<tbody>
<tr><td><h5>General Details</h5></td></tr>
<tr><td>CURRENT JOB(if any):</td><td><?= $fgmembersite->get_current_job($fgmembersite->UserEmail()); ?></td></tr>
<tr><td>WORK EXPERIENCE(if any):</td><td><?= $fgmembersite->get_work_experience($fgmembersite->UserEmail()); ?></td></tr>
<tr><td>EDUCATION :</td><td><?= $fgmembersite->get_education($fgmembersite->UserEmail()); ?></td></tr>

<tr><td>BIRTH LOCATION :</td><td><?= $fgmembersite->get_birth_place($fgmembersite->UserEmail()); ?></td></tr>
<tr><td>CURRENT LOCATION :</td><td><?=$fgmembersite->get_current_location($fgmembersite->UserEmail()); ?></td></tr>
<tr><td>BORN ON :</td><td><?= $fgmembersite->get_date($fgmembersite->UserEmail()); ?></td></tr>

<tr><td><h5>Contact Details</h5></td></tr>
<tr><td>EMAIL :</td><td><?= $fgmembersite->UserEmail(); ?></td></tr>

</tbody></table></div></div></div></div></div></div></div>
</body>
</html>