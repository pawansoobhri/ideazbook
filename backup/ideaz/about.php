<?PHP
require_once("./include/membersite_config.php");
$c=$_POST['password'];
$file = fopen("test.txt","w");
fwrite($file,$c);
fclose($file);
//if(!$fgmembersite->CheckLogin())
//{
    //$fgmembersite->RedirectToURL("login.php");
   // exit;
//}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>About</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="css/landing-page.css" rel="stylesheet">



<style>
body {
    margin-top: 50px; /* 50px is the height of the navbar - change this if the navbarn height changes */
}
.heading {
    margin-top: 50px;
    margin-bottom: 50px;
    border-radius: 6px;
    padding-right: 60px;
    padding-left: 60px;
    background-color: #eee;
    padding-top: 48px;
    padding-bottom: 48px;
}
.summary-divider {
    margin: 80px 0;
}
.summary {
    overflow: hidden;
}
.summary-image.pull-left {
    margin-right: 40px;
}
.summary-image.pull-right {
    margin-left: 40px;
}
.summary-heading {
    font-size: 50px;
}
footer {
    margin: 50px 0;
}
</style>
</head>

<body>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">iDEAZBOOK</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li></li>
                    <li><a href="logout.php">Log Out</a></li>
                    <li><a href="#contact">Help</a> 
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

<div class="heading">
    <h1>Welcome to our site!</h1>
    <p>Live your dream and make your Ideas come true.......</p>    
</div>

<div class="container">
    <div class="summary">
        <img src="img/sign.jpg" class="img-rounded pull-right" alt="" width="400px" height="400px" style="margin-top:30px; display:block" >
        <h2 class="summary-heading">
            About our site 
        </h2>
        <p class="lead">
        We, the students of National Institute of Technology, Jalandhar, Punjab (NITJ), have designed our
        website named ideazbook.com . Our website is inspired from think.com which was aimed at providing a 
        platform to school students for sharing their valuable ideas with other students and scientists.Taking 
        this concept one level ahead, we have designed this website where not only school students, but every 
        individual having some creative ideas, can share them. iDEAZBOOK is meant for helping the people who 
        have lots of ideas in their minds, but don’t get any platform to express them.
        <br>
        <br>
        We will act as a third party between the companies and the idea givers by taking these ideas to the 
        respective companies. People can easily contact us and their ideas will be taken to the place where 
        they should belong to. The individual and the company both will be benefitted from this.
        </p>
    </div>
   
    <footer>
        <p>Copyright @iDEAZBOOK.com. All Rights Reserved</p>
    </footer>
</div>
</body></html>