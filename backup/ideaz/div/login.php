<?PHP
require_once("./include/membersite_config.php");

if(isset($_POST['submitted']))
{
   if($fgmembersite->Login())
   {
        $fgmembersite->RedirectToURL("profile.php");
   }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
      <title>Login</title>
      <link rel="STYLESHEET" type="text/css" href="style/fg_membersite.css" />
      <link rel="STYLESHEET" type="text/css" href="style/button.css" />
      <script type='text/javascript' src='scripts/gen_validatorv31.js'></script>
<style type="text/css"> 
        .tbox
{
border:2px solid #b3b3b3;
background:#dddddd;
width:200px;
border-radius:25px;
-moz-border-radius:25px; 
-moz-box-shadow:    1px 1px 1px #ccc;
-webkit-box-shadow: 1px 1px 1px 1px #ccc;
 box-shadow:         1px 2px 2px 2px #ccc;
}
.berth
{
border:2px solid #b3b3b3;
background:#dddddd;
width:100px;
border-radius:25px;
-moz-border-radius:25px; 
-moz-box-shadow:    1px 1px 1px #ccc;
-webkit-box-shadow: 1px 1px 1px 1px #ccc;
 box-shadow:         1px 2px 2px 2px #ccc;
}

.se
{
border:2px solid #b3b3b3;
background:#dddddd;
width:50px;
border-radius:25px;
-moz-border-radius:25px; 
-moz-box-shadow:    1px 1px 1px #ccc;
-webkit-box-shadow: 1px 1px 1px 1px #ccc;
 box-shadow:         1px 2px 2px 2px #ccc;
}
        .style1
    {
          width: 803px;
    }

.red {
 color: red;
 }
 .blue {
 color: blue;
 }
 .yellow {
 color: yellow;
} 
 .green {
 color: green;
 }

        </style>

</head>
<body style="height:100%;overflow-y:hidden; background-color:#ffffff; margin-left:0px;margin-top:0px">
<div style="width:100%;height:50px;border-style:inset;background-color:#1c5380;border-color:#1c5380;top:0px;left:0px;position:inherit"></div>
<p style="text-align:center; font-size:15mm"><span class="blue">i</span> <span class="red">D</span> <span class="yellow">E</span> <span class="green">A</span><span class="green">B</span><span class="yellow">O</span><span class="red">O</span><span class="blue">K</span>
</p>
<!-- Form Code Start -->
</br>
<div id='fg_membersite'>
<form id='login' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>

<input type='hidden' name='submitted' id='submitted' value='1'/>



<div align="center"><span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span></div>
<div class='container' align="center">
    <label for='username' ><p style="font-size:20px; color:black">UserName*:</p></label>
<input style=
                "BACKGROUND-COLOR: #333333; text-align:center; WIDTH: 238px; HEIGHT: 34px; COLOR: #ffffff; FONT-SIZE: 25px"
                       name='username' id='username' value='<?php echo $fgmembersite->SafeDisplay('username') ?>' maxlength="50" class="tbox"><br/><br/>
    <!--<input type='text' name='username' id='username' value='<?php echo $fgmembersite->SafeDisplay('username') ?>' maxlength="50" /><br/>-->
    <span id='login_username_errorloc' class='error'></span>
</div>
<div class='container' align="center">
    <label for='password' ><p style="font-size:20px; color:black">Password*:</p></label>

<input type="password" name="password" id="password" style=
                "BACKGROUND-COLOR: #333333; text-align:center; WIDTH: 238px; HEIGHT: 34px; COLOR: #ffffff; FONT-SIZE: 25px" value='<?php echo $fgmembersite->SafeDisplay('password') ?>' maxlength="50" class="tbox" />
<br/><br/>
    
    <span id='login_password_errorloc' class='error'></span>
</div>

<div class='container' align="center">
 
   <input type='submit' name='Submit' value='Log In' class="rounded green effect-3" />
</div>
<div class='short_explanation' align="center"><a href='reset-pwd-req.php'><p style="font-size:12px; color:black">Forgot Password?</p></a></div>
<div class='short_explanation' align="center"><a href='register.php'><p style="font-size:16px; color:black">New User Registration</p></a></div>

</form>


<script type='text/javascript'>
// <![CDATA[

    var frmvalidator  = new Validator("login");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    frmvalidator.addValidation("username","req","Please provide your username");
    
    frmvalidator.addValidation("password","req","Please provide the password");

// ]]>
</script>
</div>


</body>
</html>