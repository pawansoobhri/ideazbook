-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 19, 2017 at 07:29 PM
-- Server version: 10.1.20-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id1774869_mysql`
--

-- --------------------------------------------------------

--
-- Table structure for table `reg`
--

CREATE TABLE `reg` (
  `id_user` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmcode` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_place` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_location` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dobdate` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dobmonth` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dobyear` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `education` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_experience` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reg`
--

INSERT INTO `reg` (`id_user`, `name`, `email`, `phone_number`, `username`, `password`, `confirmcode`, `image`, `address`, `birth_place`, `current_location`, `dobdate`, `dobmonth`, `dobyear`, `education`, `work_experience`, `date`) VALUES
(2, 'Pawan Soobhri', 'pawanrockz93@gmail.com', '', 'pawanrockz', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'y', 'upload/IMG_20170408_171408.jpg', NULL, 'Punjab', 'Noida Sector 62', NULL, NULL, NULL, 'Job', 'Job', '2017-05-19'),
(3, 'Divya', 'gargdivya45@gmail.com', '', 'gargdivya', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'y', 'upload/IMG_20170422_165514.jpg', NULL, 'Samrala', 'Delhi', NULL, NULL, NULL, 'B.tech', 'Job', '1995-09-11');

-- --------------------------------------------------------

--
-- Table structure for table `reg_company`
--

CREATE TABLE `reg_company` (
  `id_user` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmcode` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ceo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contactdetails` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reg_company`
--

INSERT INTO `reg_company` (`id_user`, `name`, `email`, `phone_number`, `username`, `password`, `confirmcode`, `ceo`, `about`, `address`, `contactdetails`) VALUES
(4, 'Pawan Soobhri', 'pawanrockz93@gmail.com', '', 'gargdivya', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'y', 'God', 'Nature', '', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reg`
--
ALTER TABLE `reg`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `reg_company`
--
ALTER TABLE `reg_company`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reg`
--
ALTER TABLE `reg`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `reg_company`
--
ALTER TABLE `reg_company`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
