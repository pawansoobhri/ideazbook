var express = require('express');
var app = express();

app.listen(4500, function () {
  console.log('Example app listening on port 4500!');
});

app.use('/', express.static('assets'));
app.get('/', function(req, res) {
  res.send('hello world');
});

