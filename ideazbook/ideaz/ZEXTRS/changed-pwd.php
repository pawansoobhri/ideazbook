<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Changed password</title>
      <link rel="STYLESHEET" type="text/css" href="style/fg_membersite.css">
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
      <link href="css/landing-page.css" rel="stylesheet">
      <script src="./js/jquery-1.10.2.js"></script>
</head>
<body>
      <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php include './htmlMod/nav.php';?>
            </div>
        </div>
    </nav>
      <div id='fg_membersite_content' style="margin-top: 50px;">
            <div class="container">
                  <h2>Changed password</h2>
                  Your password is updated!
                  <p><a href='logout.php'>logout</a></p>
            </div>
      </div>
<?php include './htmlMod/footer.php';?>
<script data-main="./require/js/main" src="./require/js/require.js"></script>
<script>setTimeout(function(){window.location.href='login.php'},1000);</script>
</body>
</html>
