<?PHP
require_once("./include/membersite_config.php");
//if(!$fgmembersite->CheckLogin())
//{
    //$fgmembersite->RedirectToURL("login.php");
   // exit;
//}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Privacy Policies</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href="css/landing-page.css" rel="stylesheet">
</head>

<body>
    <?php include './htmlMod/nav.php';?>  
    <div class="container" style="margin-top:50px">
<hr>
<h1>Privacy Policies</h1>
<hr>
<div>
<p><pre style="border:0px solid Black;background-color: rgb(255, 255, 255);">
<font face="Comic Sans MS" size="4">We are committed to protecting its visitors' and Users' privacy. The following Privacy and 
Cookie Policy outlines how the information  may process and how we may use that 
information to better serve visitors and Users while  using our website www.ideabook.com 
(the 'Website') . Please review the following carefully so that you understand our privacy 
practices.

This Privacy and Cookie Policy was last changed on July 9, 2014. </font>

<h3>1) GENERAL</h3>

<font face="Comic Sans MS" size="4">This Privacy Policy describes how we treat personal information received about you when 
you visit our site.
 iDEAZBOOK, an initiator to provide communication between idea inventors( idea givers)and
companies(idea seekers)  is committed to maintaining the privacy of personal information 
that you provide to us when using iDEABZOOK. This Privacy Policy describes how we treat
 personal information received about you when you visit iDEAZBOOK.</font> 

<h3>2) INFORMATION WE COLLECT</h3>

<font face="Comic Sans MS" size="4">We may collect personally identifiable information from you that may include your name,
 age, address, e-mail address, and other information.  Furthermore, we automatically 
collect non- personally identifiable IP addresses and other Web site usage information from
 you when you visit our site.</FONT>

<h3>3) HOW WE USE INFORMATION COLLECTED</h3>

<font face="Comic Sans MS" size="4">We may use information: (i) for the purposes for which you specifically provided the 
information, (ii) to send you e-mails on our behalf and on behalf of other relevant parties 
 *(iii) to provide companies and other third parties with aggregate (non-personally 
identifiable) information about our user base and usage patterns. 
We will not share your personally identifiable information outside of our site team unless
 *you “opt in” to having your personally identifiable information shared with a company 
that is not affiliated with us. However, we may disclose and use personally identifiable 
information in special circumstances where it is necessary to enforce our *Terms of Use or
 when we, in good faith, believe that the law requires us to do so. </font>
 

<h3>4) CONFIDENTIALITY AND SECURITY</h3>

<font face="Comic Sans MS" size="4">We strive to protect the privacy of the information that you share with us.  We have 
undertaken reasonable efforts to implement security measures designed to protect all
 data we collect against unauthorized access. We utilize encrypted authentication 
technology when you login to the Website in an effort to provide a secure environment 
that is protected from third party access, alteration, theft or misuse of the personal 
information gathered or confirmed at registration.
Unfortunately, no data transmission over the Internet can be guaranteed to be 100% 
secure.  As a result, although we strive to protect your information, we cannot ensure or 
warrant the security of the information that you submit to us via the Site and you do so 
at your own risk. We also implement measures to protect your personal information 
off-line. </font>

<h3>5) External Links</h3> 
<font face="Comic Sans MS" size="4">
 We may provide links to external websites whose content we believe might be of interest
 to our visitors. This privacy policy, however, does not apply to third party websites that 
 may be accessible through our Website. We do not control the privacy practices of these external
 websites and we do not conduct ongoing reviews of their privacy practices. These external
 websites might collect personally identifiable information from visitors. Before using these
 websites, we recommend that you review their user agreements and privacy policies to 
 learn how they may collect and use information about you. </font>

<h3>6) Non-Personal Information We Collect- Use of Cookies, IP Address and Other Aggregate Data</h3> 
<font face="Comic Sans MS" size="4">
 "Cookies" are small text files that are either used for the duration of a session
 ('session cookies'), or saved on a user's hard drive in order to identify that user 
 the next time he/she logs on to the Website ('persistent cookies').
 The Website uses cookies to store data about your visit. You may configure your web 
 browser to prevent cookies from being set on your computer. Cookies help us learn 
 which areas of our Website are useful to you and which areas need improvement . We 
 may also gather anonymous, or aggregate, information, which may be used by us or 
 shared with third parties. This is information does not personally identify you, but 
 may be helpful for marketing purposes or for improving the services we offer.
 We may also use your IP address (the number assigned to your computer while it is on
 the Internet) to help diagnose problems with our server, and to administer our Website.
 If you download materials from the Website, your IP address may be used to help identify
 your computer and your online history and to gather broad demographic information. </font>

<h3>7) DISCLOSURE TO COMPANIES</h3>
<font face="Comic Sans MS" size="4">
 Disclosure to companies allows us to present your idea to the company . Therefore,
 we may also use your Personal Information for presenting your idea to best of our 
 ability .  There are two ways users may receive such information from Companies, 
 either indirectly via newsletters, email or direct mail from the Site, or directly
 from the Company. We may use one or both methods depending on the Company's 
 convinience.</font>

<h3>8) PARTICIPATION, DISCUSSION and COMMENTS</h3>
<font face="Comic Sans MS" size="4">
 Certain participation, discussion and comments functions on iDEAZBOOK allow you to 
 post your own opinions and points of view and to comment on iDEAZBOOK work and 
 on what other readers have to say. iDEAZBOOK reserves the right to use such user
 -generated content, as well as any content provided to iDEAZBOOK via e-mail, for
 any purpose. When you use these features (for example, by participating in a 
 discussion or making a comment), you may be making public any information that 
 you provide including your name, location and e- mail address. iDEAZBOOK is not 
 responsible for any personal information that you make public via the Reviews’ 
 features, and has no obligation to remove any content that you made available 
 thereby. Furthermore, iDEAZBOOK is not responsible for the actions or disclosures 
 of any users of our team.</font>


<h3>9) CHANGES TO TERMS OF THIS POLICY</h3>
<font face="Comic Sans MS" size="4">
 Prior to updating or modifying this privacy policy, we will send an email to the email 
 address on file, notifying you that a change has been made to this privacy policy. 
 The email will include a summary of the changes that will be made and the effective 
 date of these changes. Your continued use of the Website after the effective date
 of the new privacy policy constitutes your acceptance of the new terms. </font>

<h3>10) CONTACT US</h3>
<font face="Comic Sans MS" size="4">
 For any enquiry you can mail us at :
     
     ideazzbook@gmail.com
 
</p>
</div></pre>
</div>

<?php include './htmlMod/footer.php';?>
<script data-main="./require/js/main" src="./require/js/require.js"></script>
</body>
</html> 


