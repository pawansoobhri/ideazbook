<?PHP require_once("./include_company/membersite_config.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Editor</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="css/landing-page.css" rel="stylesheet">
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script> 
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css"> 
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script> 
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">

<link href="dist/summernote.css" />
<script src="dist/summernote.min.js"></script>
<script src="tinymce/js/tinymce/plugins/image/plugin.min.js"></script>
<script type="text/javascript">
tinymce.PluginManager.add('example', function(editor, url) {
    // Add a button that opens a window
    editor.addButton('example', {
        text: 'My button',
        icon: false,
        onclick: function() {
            // Open window
            editor.windowManager.open({
                title: 'Example plugin',
                body: [
                    {type: 'textbox', name: 'title', label: 'Title'}
                ],
                onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    editor.insertContent('Title: ' + e.data.title);
                }
            });
        }
    });

    // Adds a menu item to the tools menu
    editor.addMenuItem('example', {
        text: 'Example plugin',
        context: 'tools',
        onclick: function() {
            // Open window with a specific url
            editor.windowManager.open({
                title: 'TinyMCE site',
                url: 'http://www.tinymce.com',
                width: 800,
                height: 600,
                buttons: [{
                    text: 'Close',
                    onclick: 'close'
                }]
            });
        }
    });
tinymce.init({
    plugins: "image",
    image_list: [ 
        {title: 'My image 1', value: 'http://www.tinymce.com/my1.gif'}, 
        {title: 'My image 2', value: 'http://www.moxiecode.com/my2.gif'} 
    ]
});
//$(document).ready(function() {
	//$('#summernote').summernote({
		//height: "500px"
	//});
//});
var postForm = function() {
	var content = $('textarea[name="content"]').html($('#summernote').code());
}
</script>
  <script>
$('#summernote').summernote({
  onImageUpload: function(files, editor, $editable) {
    console.log('image upload:', files, editor, $editable);
  }
});
$('.summernote').summernote();
$(document).ready(function() {
  $('#summernote').summernote();
});

$('.summernote').summernote({
  height: 300,                 // set editor height

  minHeight: null,             // set minimum height of editor
  maxHeight: null,             // set maximum height of editor

  focus: true,                 // set focus to editable area after initializing summernote
});
</script>

       <script src="tinymce/js/tinymce/tinymce.min.js"></script>
<script>
        tinymce.init({selector:'textarea'});
</script>
<!-- include summernote css/js-->
<link href="dist/summernote.css" />
<script src="dist/summernote.min.js"></script>

<!-- include summernote-ko-KR -->
<script src="lang/summernote-ko-KR.js">
$(document).ready(function() {
  $('#summernote').summernote({
    lang: 'ko-KR' // default: 'en-US'
  });
});
</script>
</head>
<body>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">iDEAZBOOK</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li></li>
                    <li><a href="logout_company.php">Log Out</a></li>
                    <li><a href="#contact">Help</a> 
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


<hr><div id="summernote">Hello Summernote
<textarea rows="20" name="message">Your content here.</textarea> </div>  
</body>
</html>