<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
   <div class="container">
      <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         </button>
         <a class="navbar-brand" href="./index.php">iDEAZBOOK</a>
      </div>
      <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
         <ul class="nav navbar-nav">
            <li><a href="competition.php">CompzBook</a></li>
            <li><a href="logout.php">Log Out</a></li>
            <li><a href="#contact">Help</a> 
            </li>
         </ul>
      </div>
      <!-- /.navbar-collapse -->
   </div>
</nav>
<div class="navbar navbar-inverse navbar-static-top" style="height:0px;margin-bottom: 0px;background-color:rgba(171, 190, 194, 0.25);">
   <div class="container">
      <div class="navbar-header" style="width: 100%;">
         <div class="col-xs-6 col-sm-6"><span class="navbar-brand"><?= $fgmembersite->UserFullName(); ?></span> </div>
         <div class="col-xs-6 col-sm-6">
            <form class="navbar-form navbar-left pull-right" style="border: none;" action="profileSearch.php" method="GET" role="search">
               <div class="form-group" >
                  <input type="text" style="width: 100%;background-color: #f8f8f8;" class="form-control" placeholder="Search" name="query">
               </div>
               <button type="submit" class="btn btn-default" style="margin: -10px;border-radius: 0px 5px 5px 0px;">
               <span class="glyphicon glyphicon-search" style="font-size: 0.875em;"></span>
               </button>
            </form>
         </div>
         <!--<div class="col-xs-3 col-sm-3"><span class="navbar-brand pull-right" style="font-size: 10px;"><?= date('d:m:Y h:i A')?></span></div>-->
         <!-- <div id="custom-search-input"><div class="input-group col-xs-2 col-sm-2"><input type="text" class=" search-query form-control" placeholder="Search" \> 
            <span class="input-group-btn">
            <button class="btn" type="submit">
            <i class="icon-search">
            </i>
            </button>
            </span>	</div>	
            </div>-->
      </div>
   </div>
</div>
<div class="navbar navbar-inverse navbar-static-top" style="padding-bottom:2%;padding-top:2%">
   <div class="container">
      <div class="col-xs-3 col-sm-3">
         <a href="profile.php" >  <button class="btn btn-info btn-block"><span class="fa fa-user"></span> Home </button></a>
      </div>
      <div class="col-xs-3 col-sm-3">
         <a href="update.php">   <button class="btn btn-info btn-block"><span class="fa fa-user"></span> Update Profile </button></a>
      </div>
      <div class="col-xs-3 col-sm-3">
         <a href="my_ideas.php">   <button class="btn btn-info btn-block"><span class="fa fa-user"></span> My Ideas </button></a>
      </div>
      <div class="col-xs-3 col-sm-3">
         <a href="texteditor.php">   <button class="btn btn-info btn-block" type="button"><span class="fa fa-user"></span> Editor</button> </a> 
      </div>
   </div>
</div>