<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./index.php">iDEAZBOOK</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li></li>
                    <li><a href="logout_company.php">Log Out</a></li>
                    <li><a href="#contact">Help</a> 
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        
        </div>
        </nav>
        <div class="navbar navbar-inverse navbar-static-top" style="margin-top: 30px;padding-bottom: 30px;">
         <div class="container">
            <div class="col-xs-4 col-sm-4">
               <a href="profile_company.php" >  <button class="btn btn-info btn-block"><span class="fa fa-user"></span> Home </button></a>
            </div>
            <div class="col-xs-4 col-sm-4">
               <a href="update_company.php">   <button class="btn btn-info btn-block"><span class="fa fa-user"></span> Update Profile </button></a>
            </div>
            <div class="col-xs-4 col-sm-4">
               <a href="seekideas.php">   <button class="btn btn-info btn-block" type="button"><span class="fa fa-user"></span> Seek Ideas</button> </a> 
            </div>
         </div>
      </div>