<footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li><a href="index.php">Home</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li><a href="about.php">About</a>
                        </li>
                        
                        <li class="footer-menu-divider">&sdot;</li>
                        <li><a href="contact.php">Contact</a>
                        </li>
<li class="footer-menu-divider">&sdot;</li>
                        <li><a href="policies.php">Privacy Policy</a>
                        </li>
                    </ul>
                    <p class="copyright text-muted small">Copyright @iDEAZBOOK.com. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>