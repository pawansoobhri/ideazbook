<?PHP require_once("./include/membersite_config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href="css/landing-page.css" rel="stylesheet">
<style>
    body {
        margin-top: 50px; /* 50px is the height of the navbar - change this if the navbarn height changes */
    }
    .heading {
        margin-top: 50px;
        margin-bottom: 50px;
        border-radius: 6px;
        padding-right: 60px;
        padding-left: 60px;
        background-color: #eee;
        padding-top: 48px;
        padding-bottom: 48px;
    }
    .summary-divider {
        margin: 80px 0;
    }
    .summary {
        overflow: hidden;
    }
    .summary-image.pull-left {
        margin-right: 40px;
    }
    .summary-image.pull-right {
        margin-left: 40px;
    }
    .summary-heading {
        font-size: 50px;
    }
    footer {
        margin: 50px 0;
    }
</style>
</head>

<body>
     <?php include './htmlMod/nav.php';?>
<div class="js-content" data-url="content.json">
    <div class="js-about">
        <div class="heading">
            <h1></h1><p></p>    
        </div>

        <div class="container summary">
            <img src="img/sign.jpg" class="img-rounded pull-right" alt="" width="400px" height="400px" style="margin-top:30px; display:block" >
            <h2 class="summary-heading"></h2><p class="lead callSiteBiog"></p>
        </div> 
    </div>
</div>

<?php include './htmlMod/footer.php';?>
<script data-main="./require/js/main" src="./require/js/require.js"></script>
<script src="./js/jquery-1.10.2.js"></script>
</body>
</html>