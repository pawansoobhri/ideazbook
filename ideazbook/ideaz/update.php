<?PHP
   require_once("./include/membersite_config.php");
   if(!$fgmembersite->CheckLogin())
   {
       $fgmembersite->RedirectToURL("login.php");
       exit;
   }
   if(isset($_POST['submitted']))
   {
      if($fgmembersite->ChangePassword())
      {
           $fgmembersite->RedirectToURL("update.php");
      }
   }
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Update</title>
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="assets/css/main.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
      <link href="css/landing-page.css" rel="stylesheet">
   </head>
   <body>
       <?php include './htmlMod/profileNav.php';?> 
     
      <div class="container " >
         <hr>
         <p><b>Edit Profile</b></p>
         <div class="row">
            <div class="col-md-3">
               <div class="text-center">
                  <form action="upload_save.php" method="post" enctype="multipart/form-data">
                  <div class="image-container">
            <div class="image-backdrop">
              <img  src=<?= $fgmembersite->image_path($fgmembersite->UserEmail()); ?> alt="profile pic" style="margin-top:10px; display:block" onerror="if (this.src != './upload/ib_default.PNG') this.src = './upload/ib_default.PNG';">
            </div>
          </div>
                     
                     <h6>Upload a different photo...</h6>
                     <input class="form-control" type="file" onchange="this.form.submit()" name="file" >
                     <noscript><input type="submit" name="submit" value="Profile Picture"></noscript>
            </form>
             </div>
            </div>
            <div class="col-md-9 personal-info">
               <h3>General info</h3>
               <form id='changepwd' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
                  <div class="form-group">
                     <input type='hidden' name='submitted' id='submitted' value='1'/>
                     <span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span>
                     <label class="col-lg-3 control-label">Full Name:</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->UserFullName(); ?>" name="nam" readonly>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-lg-3 control-label">Email:</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->UserEmail(); ?>" readonly>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Username:</label>
                     <div class="col-md-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->UserName($fgmembersite->UserEmail()); ?>" readonly>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Original Password:</label>
                     <div class="col-md-8">
                        <input class="form-control" type="password" value="" name='oldpwd'>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">New password:</label>
                     <div class="col-md-8">
                        <input class="form-control" type="password" value="" name='newpwd'>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label"></label>
                     <div class="col-md-8">
                        <input class="btn btn-primary" type="submit" value="Save Changes" name='Submit'>
                        <span></span>
                        <input class="btn btn-default" type="reset" value="Cancel" >
                     </div>
                  </div>
               </form>
               <h3>Personal info</h3>
               <form action="save_info.php" method="post"
                  enctype="multipart/form-data">
                  <div class="form-group">
                     <label class="col-lg-3 control-label">Birth Date:</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="date" value="<?= $fgmembersite->get_date($fgmembersite->UserEmail()); ?>" name="date">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-lg-3 control-label">Birth Place:</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->get_birth_place($fgmembersite->UserEmail()); ?>" name="birth_place">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-lg-3 control-label">Current Location:</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->get_current_location($fgmembersite->UserEmail()); ?>" name="current_location">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Education:</label>
                     <div class="col-md-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->get_education($fgmembersite->UserEmail()); ?>" name="education">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Work Experience(if any):</label>
                     <div class="col-md-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->get_work_experience($fgmembersite->UserEmail()); ?>" name="work_experience">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-lg-3 control-label">Current job:</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->get_current_job($fgmembersite->UserEmail()); ?>" name="current_job">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-lg-3 control-label">Contact No:</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->get_phonenumber($fgmembersite->UserEmail()); ?>" name="phone_number">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label"></label>
                     <div class="col-md-8">
                        <input class="btn btn-primary" type="submit" value="Save Changes" >
                        <span></span>
                        <input class="btn btn-default" type="reset" value="Cancel">
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <hr>
      </div>
   </body>
</html>