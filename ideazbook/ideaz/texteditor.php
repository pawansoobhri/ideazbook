<?PHP
   require_once("./include/membersite_config.php");
   if(!$fgmembersite->CheckLogin())
   {
       $fgmembersite->RedirectToURL("login.php");
       exit;
   }
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Text Editor</title>
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="assets/css/main.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
      <link href="css/landing-page.css" rel="stylesheet">
      <script src="//code.jquery.com/jquery-1.9.1.min.js"></script> 
      <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css">
      <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script> 
      <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
      <link href="dist/summernote.css" />
      <script src="dist/summernote.min.js"></script>
      <script src="tinymce/js/tinymce/plugins/image/plugin.min.js"></script>
      <script type="text/javascript">
         tinymce.PluginManager.add('example', function(editor, url) {
             // Add a button that opens a window
             editor.addButton('example', {
                 text: 'My button',
                 icon: false,
                 onclick: function() {
                     // Open window
                     editor.windowManager.open({
                         title: 'Example plugin',
                         body: [
                             {type: 'textbox', name: 'title', label: 'Title'}
                         ],
                         onsubmit: function(e) {
                             // Insert content when the window form is submitted
                             editor.insertContent('Title: ' + e.data.title);
                         }
                     });
                 }
             });
         
             // Adds a menu item to the tools menu
             editor.addMenuItem('example', {
                 text: 'Example plugin',
                 context: 'tools',
                 onclick: function() {
                     // Open window with a specific url
                     editor.windowManager.open({
                         title: 'TinyMCE site',
                         url: 'http://www.tinymce.com',
                         width: 800,
                         height: 600,
                         buttons: [{
                             text: 'Close',
                             onclick: 'close'
                         }]
                     });
                 }
             });
         tinymce.init({
             plugins: "image",
             image_list: [ 
                 {title: 'My image 1', value: 'http://www.tinymce.com/my1.gif'}, 
                 {title: 'My image 2', value: 'http://www.moxiecode.com/my2.gif'} 
             ]
         });
         $(document).ready(function() {
             $('#summernote').summernote({
                 height: "500px"
             });
         });
         var postForm = function() {
             var content = $('textarea[name="content"]').html($('#summernote').code());
         }
      </script>
      <script>
         $('#summernote').summernote({
         onImageUpload: function(files, editor, $editable) {
             console.log('image upload:', files, editor, $editable);
         }
         });
         $('.summernote').summernote();
         $(document).ready(function() {
         $('#summernote').summernote();
         });
         
         $('.summernote').summernote({
         height: 300,                 // set editor height
         
         minHeight: null,             // set minimum height of editor
         maxHeight: null,             // set maximum height of editor
         
         focus: true,                 // set focus to editable area after initializing summernote
         });
      </script>
      <script src="tinymce/js/tinymce/tinymce.min.js"></script>
      <script>
         tinymce.init({selector:'textarea'});
      </script>
   </head>
   <body>
      <?php include './htmlMod/profileNav.php';?>
            <div class="container" style="margin-top:50px">
        <form action="idea_to_txt.php" method="post" enctype="multipart/form-data">
            <div class="col-xs-12 col-sm-12" style="margin-bottom: 25px;">
                <div class="col-xs-6 col-sm-6">
                    <input type="text" name="title" class="form-control" placeholder="Title" style="width:100%"></input>
                </div>
                
                <div class="dropdown col-xs-6 col-sm-6">
                    <button button class="btn btn-secondary dropdown-toggle category-menu" type="button" id="dropdownMenuButton" style="background-color: whitesmoke;"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu category" style="margin-left: 15px;">
                        <li class="dropdownItem" value="ECommerce"><span class="dropdownItemSpan">ECommerce</span></li>
                        <li class="dropdownItem" value="Technical"><span class="dropdownItemSpan">Technical</span></li>
                    </ul>
                 
                </div>
        
                <input type='hidden' name='category'></input>
            </div>

                <div class="dropdown col-xs-12 col-sm-12">
                    <textarea rows="20" name="idea"></textarea>
                </div>
                <div class="form-group" style="padding-top: 33px;    margin-left: 13px;">
                    <div class="col-md-8" style="margin: 8px;margin-left: -13px;">
                        <input class="btn btn-primary" type="submit" value="Save Changes" >
                        <span></span>
                        <input class="btn btn-default" type="reset" value="Cancel">
                    </div>
                </div>
            </form>
      </div>
   </body>

<script>
        $(function(){
            //Listen for a click on any of the dropdown items
            $(".category li").click(function(){
                //Get the value
                
                var value = $(this).attr("value");
                $(".category-menu").text(value);
                //Put the retrieved value into the hidden input
                $("input[name='category']").val(value);
            });
        });
        </script>



</html>