<?PHP
   require_once("./include_company/membersite_config.php");
   
   if(!$fgmembersite->CheckLogin())
   {
       $fgmembersite->RedirectToURL("login_company.php");
       exit;
   }   
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Company Profile</title>
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="assets/css/main.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
      <link href="css/landing-page.css" rel="stylesheet">
   </head>
   <body>
      <?php include './htmlMod/companyProfileHomeNav.php';?>
      <hr>
      <div class="container">
         <div class="row">
            <div class="col-md-12" style="margin-top:30px">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <div class="image-container">
                                <div class="image-backdrop">
                                    <img  src=<?= $fgmembersite->image_path($fgmembersite->UserEmail()) ;?> alt="profile pic"  margin-top="200px" onerror="if (this.src != './upload/ib_default.PNG') this.src = './upload/ib_default.PNG';" >
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-8">
                           <h2><?= $fgmembersite->UserFullName(); ?></h2>
                           <br>
                           <table >
                              <tr style="height:25px">
                                 <td style="width: 150px;">About</td>
                                 <td style="width: 50px;">:</td>
                                 <td><?= $fgmembersite->get_about($fgmembersite->UserEmail()); ?></td>
                              </tr>
                              <tr style="height:25px">
                                 <td style="width: 150px;">Founded</td>
                                 <td style="width: 50px;">:</td>
                                 <td><?= $fgmembersite->get_founded($fgmembersite->UserEmail()); ?></td>
                              </tr>
                              <tr style="height:25px">
                                 <td style="width: 150px;">Headquarters</td>
                                 <td style="width: 50px;">:</td>
                                 <td><?= $fgmembersite->get_about($fgmembersite->UserEmail()); ?></td>
                              </tr>
                              <tr style="height:25px">
                                 <td style="width: 150px;">Founders</td>
                                 <td style="width: 50px;">:</td>
                                 <td><?php
                                    $string= $fgmembersite->get_founder($fgmembersite->UserEmail());
                                    $words = explode(',', $string);
                                    $k= count($words);
                                    for($i=0;$i<$k;++$i)
                                    {
                                    echo $words[$i];
                                    echo "<br>";
                                    }
                                    ?></td>
                              </tr>
                              <tr style="height:25px">
                                 <td style="width: 150px;">CEO Of Company</td>
                                 <td style="width: 50px;">:</td>
                                 <td><?= $fgmembersite->get_ceo($fgmembersite->UserEmail()); ?></td>
                              </tr>
                              <tr style="height:25px">
                                 <td style="width: 150px;">Contact Details</td>
                                 <td style="width: 50px;">:</td>
                                 <td><?= $fgmembersite->get_contact_details($fgmembersite->UserEmail()); ?></td>
                              </tr>
                           </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                           <p></p>
                           <a href="update_company.php" >  <button class="btn btn-success btn-block"><span class="fa fa-user"></span> Update Profile </button></a>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                           <p></p>
                           <a href="seekideas.php">   <button class="btn btn-info btn-block"><span class="fa fa-edit"></span> Seek Ideas </button></a>
                        </div>
                        <!--/col-->
                        <div class="col-xs-12 col-sm-4">
                           <p></p>
                           <a href="seekideas.php">   <button class="btn btn-primary btn-block" type="button"><span class="fa fa-question"></span> Queries </button> </a>
                        </div>
                        <!--/col-->
                     </div>
                     <!--/row-->
                  </div>
                  <!--/panel-body-->
               </div>
               <!--/panel-->
            </div>
         </div>
      </div>
   </body>
</html>