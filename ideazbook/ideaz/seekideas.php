<?PHP
   require_once("./include_company/membersite_config.php");
   
   if (!$fgmembersite->CheckLogin()) {
       $fgmembersite->RedirectToURL("login_company.php");
       exit;
   }
   
   if (isset($_POST['submitted'])) {
       if ($fgmembersite->ChangePassword()) {
           $fgmembersite->RedirectToURL("update_company.php");
       }
   }
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Company Update</title>
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="assets/css/main.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
      <link href="css/landing-page.css" rel="stylesheet">
   </head>
   <body>
      <?php include './htmlMod/companyProfileNav.php'; ?> 
      
      <div class="container " style="margin-top:30px">
         <div class="row" >
            <form class="navbar-form navbar-left" style="width: 100%;" action="" method="GET" role="search">
               <div class="form-group dropdown col-xs-9 col-sm-9" >
                  <input type="text" style="width: 100%;" class="form-control" placeholder="Search" name="query">
               </div>
               <div class="col-xs-3 col-sm-3">
                  <input type="submit" value="Search" style="width: 100%;" class="btn btn-default"></input>
               </div>
            </form>
         </div>
         <div class="row">
            <div class="col-xs-12 col-sm-12" style="margin-left: 25px;">
                
                <?php
                    $mysqli = mysqli_connect("localhost", "root", "", "ideazbook");
                    $min_length = 3;
                    if (isset($_GET['query'])) {
                        $query = $_GET['query'];
                        if (strlen($query) >= $min_length) {
                            
                            $query = htmlspecialchars($query);
                            $query = mysqli_real_escape_string($mysqli, $query);
                            $raw_results = mysqli_query($mysqli, "SELECT * FROM idea_list
                                        WHERE (`title` LIKE '%" . $query . "%') OR (`idea` LIKE '%" . $query . "%')") or die(mysql_error());
                            if (mysqli_num_rows($raw_results) > 0) {
                                echo '<div style="margin-bottom: 10px;margin-top: 10px;color: rgba(41, 128, 185, 0.71);">
                                         <span>Here are your search result</span>
                                       </div>';
                                while ($results = mysqli_fetch_array($raw_results)) {
                                    echo "<div style='margin-bottom: 10px;background-color: #f8f8f8;padding: 7px;'>
                                                <div>
                                                    <span style='font-size: 20px;color: #8c7777;'>" . ucwords($results['title']) . "</span>
                                                    <div class='pull-right' style='color: #cecece;'>
                                                    <span>Idea Id:&nbsp;&nbsp;&nbsp;"."U".$results['id_user']."I".$results['id']."</span>
                                                    <span style='margin: 0px 5px 0px 5px;'>|</span>
                                                    <span> Submitted on: ".date('d M ', strtotime($results['date']))."</span>
                                                    </div>
                                                </div>
                                                <div style='text-overflow: ellipsis;overflow: hidden;white-space: nowrap;width: 90%;'>
                                                    <span style='font-size: 13px;font-weight: 400;color: #9d9d9d;'>" . strip_tags($results['idea']) . "</span>
                                                </div>
                                            </div>";
                                }
                            } else {
                                echo "<span style='color: #e74c3c;'>No results were found for <b>".$query."</b></span>";
                            }
                        } else {
                            echo "<span style='color: #e74c3c;'>Kindly search a valid Keyword</span>";
                        }
                    } else {
                        echo "<span style='margin: 30px;color: #a2a2a2;'>Find Your Idea</span>";
                    }
                    ?>
            </div>
         </div>
      </div>
   </body>
</html>