<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>iDEAZBOOK</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href="css/landing-page.css" rel="stylesheet">
</head>

<body>
    <?php include './htmlMod/nav.php';?>
    <div class="intro-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1>iDEAZBOOK</h1>
                        <h3>A Roadway to Future</h3>
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                            <li><a href="register.php" class="btn btn-primary btn-lg"><i class="fa fa-user fa-fw"></i> <span class="network-name">iDEA Giver</span></a></li>
                            <li><a href="register_company.php" class="btn btn-primary btn-lg"><i class="fa fa-user fa-fw"></i> <span class="network-name">iDEA Seeker</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.intro-header -->

    <div class="content-section-a">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                        <h2 class="section-heading">Share your iDEAS</h2>
                        <p class="lead">Daring ideas are like chessmen moved forward. They may be beaten, but they may start a winning game.<br>There are a thousand thoughts lying within a man that he does not know till he takes up the pen to write.
                    </div>
                    <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                        <img class="img-responsive" src="img/ipad.png" alt="">
                </div>
            </div>
        </div>
    </div> 

    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Connect to iDEAZBOOK</h2>
                </div>
                <div class="col-lg-6">
                    <ul class="list-inline banner-social-buttons">
                        <li><a href="https://twitter.com/ideazbook" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                        </li>
                        <li><a href="https://www.facebook.com/ideazbook1" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Facebook</span></a>
                        </li>
                        <li><a href="https://www.linkedin.com/profile/view?id=355355262" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?php include './htmlMod/footer.php';?>
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
</body>
</html>