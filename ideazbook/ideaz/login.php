<?PHP
require_once("./include/membersite_config.php");

if(isset($_POST['submitted']))
{
   if($fgmembersite->Login())
   {
        $fgmembersite->RedirectToURL("profile.php");
   }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>iDEAZBOOK</title>
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href="./css/landing-page.css" rel="stylesheet">
</head>
<body>
<?php include './htmlMod/nav.php';?>
<div  class=" modal1 show" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog"  >
  <div class="modal-content">
      <div class="modal-header">
         <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
          <h1 class="text-center">Login</h1>
      </div>
      <div class="modal-body">
        <form id='login' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
          <input type='hidden' name='submitted' id='submitted' value='1'/>
          <span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span>
            <div class="form-group">
              <input type="text" class="form-control input-lg" placeholder="UserName" name='username' id='username' value='<?php echo $fgmembersite->SafeDisplay('username') ?>' maxlength="50">
            </div>
            <div class="form-group"> 
              <input type="password" class="form-control input-lg" placeholder="Password" name="password" id="inputPassword" value='<?php echo $fgmembersite->SafeDisplay('password') ?>' maxlength="50">
            </div>
            <div class="form-group">
              <button class="btn btn-primary btn-lg btn-block">Log In</button>
              <span class="pull-right"><a href="register.php">Register</a></span><span><a href="reset-pwd-req.php">Forgot Password?</a></span>
            </div>
          </form>
      </div>
      <div class="modal-footer"> iDEAZBOOK
          <!--<div class="col-md-12"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button></div>-->	
      </div>
  </div>
  </div>
</div>
 <script src="js/jquery-1.10.2.js"></script>
  <script src="js/bootstrap.js"></script>
</body>
</html>