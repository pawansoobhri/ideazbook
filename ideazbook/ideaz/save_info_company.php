<?php
require_once("include_company/membersite_config.php");

if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("login_company.php");
    exit;
}
$ceo=$_POST['ceo'];
$about=$_POST['about'];
$founded=$_POST['founded'];
$address=$_POST['address'];
$contact_details=$_POST['contact_details'];
//$image=$_POST['image'];
$founder=$_POST['founder'];

 $fgmembersite->save_ceo($ceo,$fgmembersite->UserEmail()); 
 $fgmembersite->save_about($about,$fgmembersite->UserEmail()); 
 $fgmembersite->save_founded($founded,$fgmembersite->UserEmail()); 
 $fgmembersite->save_address($address,$fgmembersite->UserEmail()); 
 $fgmembersite->save_contact_details($contact_details,$fgmembersite->UserEmail());
 //$fgmembersite->save_image($image,$fgmembersite->UserEmail());
 $fgmembersite->save_founder($founder,$fgmembersite->UserEmail());
 $fgmembersite->RedirectToURL("update_company.php");
?>