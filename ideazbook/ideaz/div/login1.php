<?PHP
require_once("./include/membersite_config.php");

if(isset($_POST['submitted']))
{
   if($fgmembersite->Login())
   {
        $fgmembersite->RedirectToURL("profile.php");
   }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>lOGIN</title>


<!---------------------------------------------------------------------------------------------------------------------------------->                         <link rel="STYLESHEET" type="text/css" href="style/fg_membersite.css" />
    <link rel="STYLESHEET" type="text/css" href="style/pwdwidget.css" />
    <link rel="STYLESHEET" type="text/css" href="style/button.css" />
    <link rel="STYLESHEET" type="text/css" href="style/layout.css" />
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/style.css" rel="stylesheet">    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <script type='text/javascript' src='scripts/gen_validatorv31.js'></script>
    <script src="scripts/pwdwidget.js" type="text/javascript"></script>   
    <script src="scripts/type.js" type="text/javascript"></script> 
<!---------------------------------------------------------------------------------------------------------------------------------->
       
      <script>    
function window_size()  
{ var x=screen.width; 
document.getElementById("demo").width=x;
var y=screen.height;
document.getElementById("idea1").height=y;}
      </script>
</head>
<body onload="window_size()" style="overflow-x:hidden;overflow-y:hidden;height:100%;margin-left:0px;position:relative">
      <table id="demo">
<!---------------------------------------------------------------------------------------------------------------------------------->
        <tr> 
          <td class="toolbar grad" >
            <div style="width:100%" margin-left="10%">
              <span><a href="profile.php" class="ideabook">iDEABOOK</a></span>
              <span> <a href='login.php' class="login">Help</a></span>
            </div>
           </td>
          </tr>
<!---------------------------------------------------------------------------------------------------------------------------------->
        <tr>
           <td>
<div style="padding-top:10%">            
</div>
           </td>
        </tr>
<!---------------------------------------------------------------------------------------------------------------------------------->
        <tr>
          <td >
<div id='fg_membersite' class="container img-rounded ">
<p style="color:#141414; font-family: 'Droid Serif', serif; font-size:40px;text-align:center">Please Log In</p>
  <form id='login' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
   <input type='hidden' name='submitted' id='submitted' value='1'/>
    <div align="center"><span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span>
    </div>
    <div class='container' align="center">
      <label for='username'  ><p style="font-size:20px; font-family: 'Droid Serif', serif;color:#141414">UserName</p></label><br/>
        
<input type="text" style="text-align:center; color:black;WIDTH: 238px; HEIGHT: 34px;FONT-SIZE: 25px" name='username' id='username' value='<?php echo $fgmembersite->SafeDisplay('username') ?>' maxlength="50" ><br/><br/>
    <!--<input type='text' name='username' id='username' value='<?php echo $fgmembersite->SafeDisplay('username') ?>' maxlength="50" /><br/>-->
    <span id='login_username_errorloc' class='error'></span>
   </div>
   <div class='container' align="center">
      <label for='password' ><p style="font-size:20px;font-family: 'Droid Serif', serif; color:#141414">Password</p></label>
       <br>
       <input type="password" name="password" id="inputPassword" style="text-align:center; WIDTH: 238px; HEIGHT: 34px; FONT-SIZE: 25px" value='<?php echo $fgmembersite->SafeDisplay('password') ?>' maxlength="50"  />
         <br/><br/>
    
         <span id='login_password_errorloc' class='error'></span>
   </div>

   <div class='container' align="center">
    <input type='submit' name='Submit' value=' Log In' class="btn btn-success" />
   </div>

   <div  class='container' align="center"><a href='reset-pwd-req.php'><p style="font-size:12px;font-family: 'Droid Serif', serif; color:#141414">Forgot Password?</p></a>
   </div>
   <div  class='container' align="center"><a href='register.php'><p style="font-size:16px;font-family: 'Droid Serif', serif; color:#141414">New User Registration</p></a>
   </div>
 

</form>


<script type='text/javascript'>
// <![CDATA[

    var frmvalidator  = new Validator("login");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    frmvalidator.addValidation("username","req","Please provide your username");
    
    frmvalidator.addValidation("password","req","Please provide the password");

// ]]>
</script>
</div>
          </td>
        </tr>
<tr> 
          <td class="toolbar grad" style="top:93%;height:30px">
            <div style="width:100%" margin-left="10%">
                          <span><a href="profile.php" class="ideabook" style="font-size:16px">Powered By: iDEABOOK</a></span>
              <span> <a href='login.php' class="login" style="top:10px">Contact Us</a></span>
              <span> <a href='login.php' class="login" style="top:10px; right:198px">Any Suggestions</a></span>
            </div>
           </td>
          </tr>

</table>






<script src="bootstrap/jquery/jquery-2.1.0.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>