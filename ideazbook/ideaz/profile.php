<?PHP
   require_once("./include/membersite_config.php");
   if(!$fgmembersite->CheckLogin())
   {
       $fgmembersite->RedirectToURL("login.php");
      exit;
      }
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
      <title> <?= $fgmembersite->UserFullName(); ?></title>
      <!--<link href="assets/css/bootstrap.css" rel="stylesheet">-->
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="assets/css/main.css" rel="stylesheet">
      <link href="css/landing-page.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
      <script src="assets/js/hover.zoom.js"></script>
      <script src="assets/js/hover.zoom.conf.js"></script>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   </head>
   <body>
      <div class="main-wrapper">
         <div class="main-container">
            <?php include './htmlMod/profileNav.php';?> 
            <div class="container">
               <div class="row">
                  <div style="margin-left: 0%;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-3 col-sm-offset-3 col-md-offset-0 col-lg-offset-0 toppad">
                     <div class="panel panel-info">
                        <div class="panel-body">
                           <div class="row">
                              <div class="col-md-4 col-lg-4">
                               <div class="image-container">
            <div class="image-backdrop">
              <img  src=<?= $fgmembersite->image_path($fgmembersite->UserEmail()) ;?> alt="profile pic"  margin-top="200px" onerror="if (this.src != './upload/ib_default.PNG') this.src = './upload/ib_default.PNG';" >
            </div>
          </div>
          </div>
                              <div class="col-md-7 col-lg-7">
                                 <table class="table table-user-information">
                                    <tbody>
                                       <tr>
                                          <td style="border-top: none">
                                             <h5>GENERAL DETAILS</h5>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>Current Job(if any):</td>
                                          <td><?= $fgmembersite->get_current_job($fgmembersite->UserEmail()); ?></td>
                                       </tr>
                                       <tr>
                                          <td>Work Experience(if any):</td>
                                          <td><?= $fgmembersite->get_work_experience($fgmembersite->UserEmail()); ?></td>
                                       </tr>
                                       <tr>
                                          <td>Eductaion :</td>
                                          <td><?= $fgmembersite->get_education($fgmembersite->UserEmail()); ?></td>
                                       </tr>
                                       <tr>
                                          <td>Birth Location :</td>
                                          <td><?= $fgmembersite->get_birth_place($fgmembersite->UserEmail()); ?></td>
                                       </tr>
                                       <tr>
                                          <td>Current Location :</td>
                                          <td><?=$fgmembersite->get_current_location($fgmembersite->UserEmail()); ?></td>
                                       </tr>
                                       <tr>
                                          <td>DOB :</td>
                                          <td><?= $fgmembersite->get_date($fgmembersite->UserEmail()); ?></td>
                                       </tr>
                                       <tr>
                                          <td>Contact Details :</td>
                                          <td><?= $fgmembersite->get_phonenumber($fgmembersite->UserEmail()); ?></td>
                                       </tr>
                                       <tr>
                                          <td>Email :</td>
                                          <td><?= $fgmembersite->UserEmail(); ?></td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php include './htmlMod/footer.php';?>
         </div>
      </div>
      <script data-main="./require/js/main" src="./require/js/require.js"></script>
      <script src="./js/jquery-1.10.2.js"></script>
   </body>
</html>