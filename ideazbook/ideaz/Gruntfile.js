module.exports = function (grunt) {

	// Project configuration.
	grunt.initConfig({
		//pkg: grunt.file.readJSON('package.json'),
		uglify: {
			build: {
				src: './require/js/*.js',
				dest: './require/js/s.min.js'
			}
		},
		typescript: {
    base: {
      src: ['require/ts/scripts/*.ts'],
      dest: 'require/js',
     
    } 
  },
		watch: {
			files: ['require/ts/scripts/*.ts'],
			tasks: ['typescript'],
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-typescript');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('default', ['watch']);

};