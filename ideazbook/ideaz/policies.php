<?PHP
   require_once("./include/membersite_config.php");
   //if(!$fgmembersite->CheckLogin())
   //{
       //$fgmembersite->RedirectToURL("login.php");
      // exit;
   //}
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Privacy Policies</title>
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
      <link href="css/landing-page.css" rel="stylesheet">
   </head>
   <body>
      <?php include './htmlMod/nav.php';?>  
      <div class="container " style="margin-top:50px">
         <hr>
         <h1>Privacy Policies</h1>
         <hr>
         <div class="js-policies" data-url="content.json">
            <div class="js-data">
               <p>
                  <!--<pre style="border:0px solid Black;background-color: rgb(255, 255, 255);">-->
                  <font face=""  class="desc"></font>
               <h3>1) General</h3>
               <font face="" class="general"></font> 
               <h3>2) Information We Collect</h3>
               <font face=""  class="informationWeCollect"></FONT>
               <h3>3) How we use Information collected?</h3>
               <font face=""   class="useInfo"></font>
               <h3>4) Confidentially & Security</h3>
               <font face=""    class="confidential"> </font>
               <h3>5) External Links</h3>
               <font face=""   class="externalLinks"></font>
               <h3>6) Non-Personal Information We Collect- Use of Cookies, IP Address and Other Aggregate Data</h3>
               <font face=""   class="nonPersonalInfo"></font>
               <h3>7) Disclosure to Companies</h3>
               <font face=""  class="disclosure"></font>
               <h3>8) Participation, Discussion & Comments</h3>
               <font face=""   class="participation"></font>
               <h3>9) Changes to Terms of this Policy</h3>
               <font face=""   class="term"></font>
               <h3>10) Contact Us</h3>
               <font face=""   class="contact"></font>
               <!--</pre>-->
               </p>
            </div>
         </div>
      </div>
      <?php include './htmlMod/footer.php';?>
      <script data-main="./require/js/main" src="./require/js/require.js"></script>
      <script src="./js/jquery-1.10.2.js"></script>
   </body>
</html>