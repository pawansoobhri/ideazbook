<?PHP
   require_once("./include/membersite_config.php");
   $emailsent = false;
   if(isset($_POST['submitted']))
   {
      if($fgmembersite->EmailResetPasswordLink())
      {
           $fgmembersite->RedirectToURL("reset-pwd-link-sent.html");
           exit;
      }
   }
   ?>
<script type='text/javascript' src='./scripts/gen_validatorv31.js'></script>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>iDEAZBOOK</title>
      <link rel="STYLESHEET" type="text/css" href="./style/fg_membersite.css" />
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
      <link href="css/landing-page.css" rel="stylesheet">
   </head>
   <body>
      <?php include 'htmlMod/profileNav.php';?>
      <!-- Form Code Start -->
      <div  class=" modal1 show" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog"  >
         <div class="modal-content">
            <div class="modal-header">
               <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
               <h1 class="text-center">Reset Password</h1>
            </div>
            <div class="modal-body" >
               <form id='resetreq' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
                  <input type='hidden' name='submitted' id='submitted' value='1'/>
                  <span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span>
                  <div class="form-group">
                     <span id='resetreq_email_errorloc' class='error'></span>
                     <input type="text" class="form-control input-lg" placeholder="Email" name='email' id='email' value='<?php echo $fgmembersite->SafeDisplay('username') ?>' maxlength="50">
                  </div>
                  <div class='short_explanation'>A link to reset your password will be sent to the email address</div>
                  <div class="form-group">
                     <button class="btn btn-primary btn-lg btn-block">Reset</button>
                  </div>
                  <div class="modal-footer">
                     iDEAZBOOK<span class="pull-left"><a href="login.php">Log In</a></span>
                     <!--<div class="col-md-12">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>-->	
                  </div>
               </form>
            </div>
         </div>
         <script type='text/javascript'>
            var frmvalidator  = new Validator("resetreq");
            frmvalidator.EnableOnPageErrorDisplay();
            frmvalidator.EnableMsgsTogether();
            frmvalidator.addValidation("email","req","Please provide the email address used to sign-up");
            frmvalidator.addValidation("email","email","Please provide the email address used to sign-up");
         </script>
      </div>
      <?php include './htmlMod/footer.php';?>
      <script src="js/jquery-1.10.2.js"></script>
      <script src="js/bootstrap.js"></script>
   </body>
</html>