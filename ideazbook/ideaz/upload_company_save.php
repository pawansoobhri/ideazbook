<?php
require_once("./include_company/membersite_config.php");
function redirect($url)
{
    if (!headers_sent()) {
        header('Location: ' . $url);
        exit;
    } else {
        exit;
    }
}
if (!$fgmembersite->CheckLogin()) {
    redirect("login_company.php");
    exit;
}
$allowedExts   = array(
    "gif",
    "jpeg",
    "JPG",
    "jpg",
    "png"
);
$temp          = explode(".", $_FILES["file"]["name"]);
$extension     = end($temp);
$directoryName = "./upload/ideaSeeker/" . $fgmembersite->get_iduser($fgmembersite->UserEmail())."/"."image/";

if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/JPG") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/pjpeg") || ($_FILES["file"]["type"] == "image/x-png") || ($_FILES["file"]["type"] == "image/png")) && ($_FILES["file"]["size"] < 20000000000000) && in_array($extension, $allowedExts)) {
    if ($_FILES["file"]["error"] > 0) {
        // echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
    } else {
        // echo "Upload: " . $_FILES["file"]["name"] . "<br>";
        // echo "Type: " . $_FILES["file"]["type"] . "<br>";
        //  echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
        // echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";
        if (!is_dir($directoryName)) {
            
            mkdir( $directoryName, 0755, true);
            if (file_exists($directoryName . "/" . $_FILES["file"]["name"])) {
                echo $_FILES["file"]["name"] . " already exists. ";
            } else {
                move_uploaded_file($_FILES["file"]["tmp_name"], $directoryName . "/" . $_FILES["file"]["name"]);
                $url = $directoryName . "/" . $_FILES["file"]["name"];
                $fgmembersite->save_image($url, $fgmembersite->UserEmail());
                // echo "Stored in: " . "upload/" . $_FILES["file"]["name"];
                
                redirect("update_company.php");
            }
        } else {
            if (file_exists($directoryName . "/" . $_FILES["file"]["name"])) {
                echo $_FILES["file"]["name"] . " already exists. ";
            } else {
                move_uploaded_file($_FILES["file"]["tmp_name"],  $directoryName . "/" . $_FILES["file"]["name"]);
                $url = $directoryName . "/" . $_FILES["file"]["name"];
                $fgmembersite->save_image($url, $fgmembersite->UserEmail());
                // echo "Stored in: " . "upload/" . $_FILES["file"]["name"];
                
                redirect("update_company.php");
            }
        }
    }
} else {
    echo "Invalid file";
}
?>