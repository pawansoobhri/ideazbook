<?php
class competition_conn{
    
    function __construct(){
        //database configuration
        $mysqli = new mysqli("localhost", "root", "", "ideazbook");
        
        if($mysqli->connect_errno){
            echo "Failed to connect to database: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        }else{
            $this->db = $mysqli;
            // echo "Connected";
            // echo $_SERVER['REMOTE_ADDR'];
        }
    }
    
    function get_rows($id = ''){
        if($id != ''){
            //fetch single row
            $query = $this->db->query("SELECT * FROM comp WHERE comp_id = $id");
            $data = $query->fetch_assoc();
        }else{
            //fetch all rows
            $query = $this->db->query("SELECT * FROM comp");
            if($query->num_rows > 0){

                while($row =  $query->fetch_assoc()){
                    $data[] = $row;
                }
            }else{
                $data = array();
            }
        }
        return $data;
    }
    
    function update($data = array(), $conditions = array()){
        $data_array_num = count($data);
        $cols_vals = "";
        $condition_str = "";
        $i=0;
        foreach($data as $key=>$val){
            $i++;
            $sep = ($i == $data_array_num)?'':', ';
            $cols_vals .= $key."='".$val."'".$sep;
        }
        foreach($conditions as $key=>$val){
            $i++;
            $sep = ($i == $data_array_num)?"":" AND ";
            $condition_str .= $key."='".$val."'";
        }
        //update data
        $update = $this->db->query("UPDATE comp SET $cols_vals WHERE $condition_str");
        return $update?TRUE:FALSE;
    }
}
?>