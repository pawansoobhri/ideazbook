/// <reference path="../def/lib/jquery.d.ts" />
/// <reference path="../def/lib/require.d.ts" />
function optionalComponents() {
    if (document.getElementsByClassName("js-content").length) {
        requirejs(["content"]);
    }
    if (document.getElementsByClassName("js-policies").length) {
        requirejs(["policies"]);
    }
    if (document.getElementsByClassName("js-readTxtFile").length) {
        requirejs(["readTxtFile"]);
    }
}
optionalComponents();
