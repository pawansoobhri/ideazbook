/// <reference path="../def/lib/jquery.d.ts" />
var MultiselectDropdownManager = (function () {
    function MultiselectDropdownManager() {
        this.rootSelector = ".js-multiselectDropdown";
        this.rootElementList = null;
        var base = this;
        require(['managers/multiselectdropdown'], function (MultiselectDropdown) {
            base.rootElementList = document.querySelectorAll(base.rootSelector);
            window["MultiselectDropdownManager"] = {};
            for (var i = 0; i < base.rootElementList.length; i++) {
                new MultiselectDropdown(base.rootElementList[i]);
            }
        });
    }
    return MultiselectDropdownManager;
})();
new MultiselectDropdownManager;
/*<div class="eG2Col blog-inline-form">
                        <label>Filter:</label>
                        <div class="js-multiselectDropdown">
                         
                            <div style="background-color: white">
                                <div class="action-btn " data-action="colex"><span>Choose Tag</span></div>

                                <div class="eG2Col list-show" data-action="option-tray">
                                    <ul></ul>
                                </div>

                                <div data-action="checkbox-tray" class="hidden">
                                    <input type="checkbox" value="5G" />5G</br>
                                    <input type="checkbox" value="Connectivity"  />Connectivity</br>
                                    <input type="checkbox" value="Digital transformation" />Digital transformation</br>
                                    <input type="checkbox" value="Radio system"  />Radio system</br>
                                    <input type="checkbox" value="Mobile"  />Mobile</br>
                                    
                                </div>
                            </div>
                        </div>
                    </div>*/ 
