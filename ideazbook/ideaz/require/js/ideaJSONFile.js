/// <reference path="../def/lib/jquery.d.ts" />
var ideaJSONFileManager = (function () {
    function ideaJSONFileManager() {
        this.rootSelectionString = ".js-readTxtFile";
        this.feed = null;
        var base = this;
        base.$rootElement = $(base.rootSelectionString);
        base.contentValidate();
    }
    ideaJSONFileManager.prototype.populateContentFeed = function (data) {
        var base = this;
        JSON.stringify(data[0].about.title);
        var lastContentBlock = null;
        lastContentBlock = base.$rootElement.find('.js-about').last();
        lastContentBlock.find('.heading h1').text(data[0].about.title);
    };
    ideaJSONFileManager.prototype.arrangeJSON = function (data) {
        console.log(JSON.stringify(data[0].about.title));
        var base = this;
        base.populateContentFeed(data);
    };
    ideaJSONFileManager.prototype.jsonContentFeed = function (jsonURL, callback) {
        //console.log(callback);
        var base = this;
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: jsonURL,
            success: function (data) { callback(data); console.log("Success"); },
            error: function (errorObj) { console.log(errorObj); console.log("Error"); }
        });
    };
    ideaJSONFileManager.prototype.contentValidate = function () {
        var base = this;
        if (base.$rootElement.length !== 0) {
            base.jsonURL = $(".js-readTxtFile").data('url');
            base.jsonContentFeed(base.jsonURL, function (data) {
                if (!$.isEmptyObject(data)) {
                    console.log("data");
                }
                if ($.isEmptyObject(data)) {
                }
            });
        }
    };
    return ideaJSONFileManager;
})();
new ideaJSONFileManager();
