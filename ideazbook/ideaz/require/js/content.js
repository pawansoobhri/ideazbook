/// <reference path="../def/lib/jquery.d.ts" />
var contentFeedManager = (function () {
    function contentFeedManager() {
        this.rootSelectionString = ".js-content";
        this.feed = null;
        var base = this;
        base.$rootElement = $(base.rootSelectionString);
        base.contentValidate();
    }
    contentFeedManager.prototype.populateContentFeed = function (data) {
        var base = this;
        JSON.stringify(data[0].about.title);
        var lastContentBlock = null;
        lastContentBlock = base.$rootElement.find('.js-about').last();
        lastContentBlock.find('.heading h1').text(data[0].about.title);
        lastContentBlock.find('.heading p').text(data[0].about.tagline);
        lastContentBlock.find('.summary h2').text(data[0].about.descTitle);
        lastContentBlock.find('.summary p').text(data[0].about.desc);
    };
    contentFeedManager.prototype.arrangeJSON = function (data) {
        console.log(JSON.stringify(data[0].about.title));
        var base = this;
        base.populateContentFeed(data);
    };
    contentFeedManager.prototype.jsonContentFeed = function (jsonURL, callback) {
        console.log(callback);
        var base = this;
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: jsonURL,
            success: function (data) { callback(data); console.log("Success"); },
            error: function (errorObj) { console.log(errorObj); console.log("Error"); }
        });
    };
    contentFeedManager.prototype.contentValidate = function () {
        var base = this;
        if (base.$rootElement.length !== 0) {
            base.jsonURL = $(".js-content").data('url');
            base.jsonContentFeed(base.jsonURL, function (data) {
                if (!$.isEmptyObject(data)) {
                    base.arrangeJSON(data);
                }
                if ($.isEmptyObject(data)) {
                }
            });
        }
    };
    return contentFeedManager;
})();
new contentFeedManager();
