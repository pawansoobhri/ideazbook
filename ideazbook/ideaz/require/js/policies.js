/// <reference path="../def/lib/jquery.d.ts" />
var policiesManager = (function () {
    function policiesManager() {
        this.rootSelectionString = ".js-policies";
        this.feed = null;
        var base = this;
        base.$rootElement = $(base.rootSelectionString);
        base.policiesValidate();
    }
    policiesManager.prototype.populatePoliciesFeed = function (data) {
        var base = this;
        console.log(JSON.stringify(data[0].privacyPolicies.desc));
        console.log(data[0].privacyPolicies.desc);
        var lastPoliciesBlock = null;
        lastPoliciesBlock = base.$rootElement.find('.js-data').last();
        console.log("Pawan");
        console.log(lastPoliciesBlock);
        lastPoliciesBlock.find('.desc').text(data[0].privacyPolicies.desc);
        lastPoliciesBlock.find('.general').text(data[0].privacyPolicies.general);
        lastPoliciesBlock.find('.informationWeCollect').text(data[0].privacyPolicies.informationWeCollect);
        lastPoliciesBlock.find('.useInfo').text(data[0].privacyPolicies.useInfo);
        lastPoliciesBlock.find('.confidential').text(data[0].privacyPolicies.confidential);
        lastPoliciesBlock.find('.externalLinks').text(data[0].privacyPolicies.externalLinks);
        lastPoliciesBlock.find('.nonPersonalInfo').text(data[0].privacyPolicies.nonPersonalInfo);
        lastPoliciesBlock.find('.disclosure').text(data[0].privacyPolicies.disclosure);
        lastPoliciesBlock.find('.participation').text(data[0].privacyPolicies.participation);
        lastPoliciesBlock.find('.term').text(data[0].privacyPolicies.term);
        lastPoliciesBlock.find('.contact').text(data[0].privacyPolicies.contact);
    };
    policiesManager.prototype.arrangeJSON = function (data) {
        console.log(JSON.stringify(data[0].privacyPolicies.term));
        var base = this;
        base.populatePoliciesFeed(data);
    };
    policiesManager.prototype.jsonPoliciesFeed = function (jsonURL, callback) {
        console.log(callback);
        var base = this;
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: jsonURL,
            success: function (data) { callback(data); console.log("Success"); },
            error: function (errorObj) { console.log(errorObj); console.log("Error"); }
        });
    };
    policiesManager.prototype.policiesValidate = function () {
        var base = this;
        if (base.$rootElement.length !== 0) {
            base.jsonURL = $(".js-policies").data('url');
            base.jsonPoliciesFeed(base.jsonURL, function (data) {
                if (!$.isEmptyObject(data)) {
                    base.arrangeJSON(data);
                }
                if ($.isEmptyObject(data)) {
                }
            });
        }
    };
    return policiesManager;
})();
new policiesManager();
