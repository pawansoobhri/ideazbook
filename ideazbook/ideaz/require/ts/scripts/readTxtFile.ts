/// <reference path="../def/lib/jquery.d.ts" />

class readTxtFileManager {
    "use strict"   
private rootSelectionString: string = ".js-readTxtFile";
    private $rootElement: JQuery;

    constructor() {
        var base = this;
         base.$rootElement = $(base.rootSelectionString);
         
        var file_url;
        file_url=$(".js-readTxtFile").data('url');
        console.log(file_url);
        base.fileDisplayArea(file_url);
    }

private fileDisplayArea = function(file)
{
    var base=this;
    var displayIdea = document.getElementsByClassName('js-displayIdea');
    console.log( base.displayIdea)
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                //$(".js-displayIdea").html(allText);
                base.$rootElement.find('.js-displayIdea').html(allText);
                //base.displayIdea.innerHTML = allText ;
                console.log(allText)
            }
        }
    }
    rawFile.send(null);
}
}
new readTxtFileManager();
