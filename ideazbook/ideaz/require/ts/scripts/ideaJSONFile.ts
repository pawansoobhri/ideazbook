/// <reference path="../def/lib/jquery.d.ts" />

class ideaJSONFileManager {
    "use strict";
    private rootSelectionString: string = ".js-readTxtFile";
    private $rootElement: JQuery;
    private jsonURL;
    private feed = null;
    

    constructor() {
        var base = this;
        base.$rootElement = $(base.rootSelectionString);
        base.contentValidate();
    }

    private populateContentFeed(data){
        var base=this;
        JSON.stringify(data[0].about.title);
        var lastContentBlock = null;
        lastContentBlock = base.$rootElement.find('.js-about').last();                
        lastContentBlock.find('.heading h1').text(data[0].about.title);     

    }

    private arrangeJSON(data) {
        console.log(JSON.stringify(data[0].about.title));
        var base = this;
        base.populateContentFeed(data);
    }

    private jsonContentFeed(jsonURL, callback) {
        //console.log(callback);
        var base = this;
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: jsonURL,
            success: function (data) { callback(data); console.log("Success"); },
            error: function (errorObj) { console.log(errorObj); console.log("Error"); }
        });
    }

    private contentValidate() {
        var base = this;
        if (base.$rootElement.length !== 0) {
            base.jsonURL = $(".js-readTxtFile").data('url');
            base.jsonContentFeed(base.jsonURL, function (data) {
                if (!$.isEmptyObject(data)) {
                    console.log("data");
                    //base.arrangeJSON(data);
                }
                if ($.isEmptyObject(data)) {
                    //base.faultyJSON("Empty JSON");
                }
            });
        }
    }
}

new ideaJSONFileManager();