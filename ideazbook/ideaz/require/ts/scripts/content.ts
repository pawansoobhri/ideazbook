/// <reference path="../def/lib/jquery.d.ts" />

class contentFeedManager {
    "use strict";
    private rootSelectionString: string = ".js-content";
    private $rootElement: JQuery;
    private jsonURL;
    private feed = null;
    

    constructor() {
        var base = this;
        base.$rootElement = $(base.rootSelectionString);
        base.contentValidate();
    }

    private populateContentFeed(data){
        var base=this;
        JSON.stringify(data[0].about.title);
        var lastContentBlock = null;
        lastContentBlock = base.$rootElement.find('.js-about').last();                
        lastContentBlock.find('.heading h1').text(data[0].about.title);     
        lastContentBlock.find('.heading p').text(data[0].about.tagline);  
        lastContentBlock.find('.summary h2').text(data[0].about.descTitle);
        lastContentBlock.find('.summary p').text(data[0].about.desc);     
    }

    private arrangeJSON(data) {
        console.log(JSON.stringify(data[0].about.title));
        var base = this;
        base.populateContentFeed(data);
    }

    private jsonContentFeed(jsonURL, callback) {
        console.log(callback);
        var base = this;
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: jsonURL,
            success: function (data) { callback(data); console.log("Success"); },
            error: function (errorObj) { console.log(errorObj); console.log("Error"); }
        });
    }

    private contentValidate() {
        var base = this;
        if (base.$rootElement.length !== 0) {
            base.jsonURL = $(".js-content").data('url');
            base.jsonContentFeed(base.jsonURL, function (data) {
                if (!$.isEmptyObject(data)) {
                    base.arrangeJSON(data);
                }
                if ($.isEmptyObject(data)) {
                    //base.faultyJSON("Empty JSON");
                }
            });
        }
    }
}

new contentFeedManager();