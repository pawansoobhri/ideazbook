<?PHP
   require_once("./include/membersite_config.php");
   if(!$fgmembersite->CheckLogin())
   {
       $fgmembersite->RedirectToURL("login.php");
      exit;
      }
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
      <title> <?= $fgmembersite->UserFullName(); ?></title>
      <!--<link href="assets/css/bootstrap.css" rel="stylesheet">-->
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="assets/css/main.css" rel="stylesheet">
      <link href="css/landing-page.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
      <script src="assets/js/hover.zoom.js"></script>
      <script src="assets/js/hover.zoom.conf.js"></script>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   </head>
   <body>
      <div class="main-wrapper">
         <div class="main-container">
            <?php include './htmlMod/profileNav.php';?> 
            
            
            <div class="container">
               <div class="row" style="margin: 8px 0px 8px -26px;">
                  <div class="col-xs-12 col-sm-12" style="margin-left: 13px;">
               <?php
                  $mysqli = mysqli_connect("localhost", "root", "", "ideazbook");
                  $min_length = 3;
                  if (isset($_GET['query'])) {
                      $query = $_GET['query'];
                      if (strlen($query) >= $min_length) {
                          
                          $query = htmlspecialchars($query);
                          $query = mysqli_real_escape_string($mysqli, $query);
                          $raw_results = mysqli_query($mysqli, "SELECT * FROM reg
                                      WHERE (`name` LIKE '%".$query."%') OR (`email` LIKE '%".$query."%')") or die(mysqli_error($mysqli));
                          
                          if (mysqli_num_rows($raw_results) > 0) {
                              while ($results = mysqli_fetch_array($raw_results)) {
                                  $count=mysqli_query($mysqli,"SELECT Count(id_user) as idea_count FROM idea_list
                                      WHERE (`id_user` LIKE '%".$results['id_user']."%')") or die(mysqli_error($mysqli));
                                  $countdata=mysqli_fetch_assoc($count);
                                  echo "<div class='col-xs-6 col-sm-6'>";
                                  echo "<div class='col-xs-6 col-sm-6' style='width: 90px;height: 60px;'><img  style='width: 100%;height: 100%;'src=" .$results['image']. "></div>";
                                  echo "<div style='color: #777777;' class='col-xs-4 col-sm-4'><div><span>" . $results['name'] . "</span></div><div>" . $results['phone_number'] . "</div><div><span style='margin-right: 22px;'>Total Ideas</span>" . $countdata['idea_count'] . "</div></div>";
                                  echo "</div>";
                              }
                          } else {
                              echo "No results";
                          }
                      } else {
                          echo "<span style='color: #a2a2a2;'>Minimum length is " . $min_length . "</span>";
                      }
                  } else {
                      echo "<span style='margin: 30px;color: #a2a2a2;'>Find Your Idea</span>";
                  }
                  ?>
               </div>
            </div>
            <?php include './htmlMod/footer.php';?>
         </div>
      </div>
      <script data-main="./require/js/main" src="./require/js/require.js"></script>
      <script src="./js/jquery-1.10.2.js"></script>
   </body>
</html>