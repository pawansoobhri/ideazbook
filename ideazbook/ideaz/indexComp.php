<?php
   include_once("competition_conn.php");
   require_once("./include/membersite_config.php");
      if(!$fgmembersite->CheckLogin())
      {
          $fgmembersite->RedirectToURL("login.php");
         exit;
         }
   $comp = new competition_conn();
   $trows = $comp->get_rows();
   $mysqli = new mysqli("localhost", "root", "", "ideazbook");
   $select_user_voted = "SELECT * FROM user_voted";
   $result_user_voted = $mysqli->query($select_user_voted);
   
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
      <title> <?= $fgmembersite->UserFullName(); ?></title>
      <!--<link href="assets/css/bootstrap.css" rel="stylesheet">-->
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="assets/css/main.css" rel="stylesheet">
      <link href="css/landing-page.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
      <script src="assets/js/hover.zoom.js"></script>
      <script src="assets/js/hover.zoom.conf.js"></script>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   </head>
   <body>
      <div class="main-wrapper">
      <div class="main-container">
      <?php include './htmlMod/profileNav.php';?> 
      <div class="container">
      <div class="row">
      <div style="margin-left: 0%;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-3 col-sm-offset-3 col-md-offset-0 col-lg-offset-0 toppad">
      <div class="panel panel-info">
      <div class="panel-body">
      <?php foreach($trows as $trow){  ?>
      <div class='col-xs-4 col-sm-4' style="background-color: #eaeff0;">
         <img src=<?php echo $trow['image']; ?> alt="" style='width:100%' />
         <div >
            <?php 
               $id_user = $fgmembersite->get_iduser($fgmembersite->UserEmail());
               if ($result_user_voted->num_rows > 0) {
                   while($row_user_voted = $result_user_voted->fetch_assoc()) {
                      if($row_user_voted['user_id'] == $id_user && $row_user_voted['vote'] == '1' && $row_user_voted['downvote'] == '0')
                      {?>
            <div class='col-xs-12 col-sm-12'>
               <div class='col-xs-6 col-sm-6'>
                  <span class="glyphicon glyphicon-thumbs-up" style="margin-top:5px"></span>&nbsp;
                  <span class="counter" id="like_count<?php echo $trow['comp_id'];?>"><?php echo $trow['like_num'];?></span>&nbsp;&nbsp;&nbsp;
               </div>
               <div class='col-xs-6 col-sm-6' style="padding-left: 80px;">
                  <span class="glyphicon glyphicon-thumbs-down" onClick="cwRating(<?php echo $trow['comp_id']?>,0,'dislike_count<?php echo $trow['comp_id'];?>')"></span>&nbsp;
                  <span class="counter" id="dislike_count<?php echo $trow['comp_id'];?>"><?php echo $trow['dislike_num'];?></span>
               </div>
            </div>
            <?php 
               } 
               
               elseif($row_user_voted['user_id'] == $id_user && $row_user_voted['vote'] == '0' && $row_user_voted['downvote'] == '1') {
                   ?>
            <div class='col-xs-12 col-sm-12' >
               <div class='col-xs-6 col-sm-6'>
                  <span class="glyphicon glyphicon-thumbs-up" style="margin-top:5px" onClick="cwRating(<?php echo $trow['comp_id']?>,1,'like_count<?php echo $trow['comp_id'];?>')"></span>&nbsp;
                  <span class="counter" id="like_count<?php echo $trow['comp_id'];?>"><?php echo $trow['like_num'];?></span>&nbsp;&nbsp;&nbsp;
               </div>
               <div class='col-xs-6 col-sm-6' style="padding-left: 80px;">
                  <span class="glyphicon glyphicon-thumbs-down" style="margin-top:5px"></span>&nbsp;
                  <span class="counter" id="dislike_count<?php echo $trow['comp_id'];?>"><?php echo $trow['dislike_num'];?></span>
               </div>
            </div>
            <?php } 
               
                   else {?>
            <div class='col-xs-12 col-sm-12' >
               <div class='col-xs-6 col-sm-6'>
                  <span class="glyphicon glyphicon-thumbs-up" style="margin-top:5px" onClick="cwRating(<?php echo $trow['comp_id']?>,1,'like_count<?php echo $trow['comp_id'];?>')"></span>&nbsp;
                  <span class="counter" id="like_count<?php echo $trow['comp_id'];?>"><?php echo $trow['like_num'];?></span>&nbsp;&nbsp;&nbsp;
               </div>
               <div class='col-xs-6 col-sm-6' style="padding-left: 80px;">
                  <span class="glyphicon glyphicon-thumbs-down" onClick="cwRating(<?php echo $trow['comp_id']?>,0,'dislike_count<?php echo $trow['comp_id'];?>')"></span>&nbsp;
                  <span class="counter" id="dislike_count<?php echo $trow['comp_id'];?>"><?php echo $trow['dislike_num'];?></span>
               </div>
            </div>
            <?php }  } }?>  
            
         </div>
      </div>
      <?php } ?>
      <script data-main="./require/js/main" src="./require/js/require.js"></script>
      <script src="./js/jquery-1.10.2.js"></script>
      <script type="text/javascript">
         function cwRating(id,type,target){
             $.ajax({
                 type:'POST',
                 url:'competition_rating.php',
                 data:'comp_id='+id+'&type='+type,
                 success:function(msg){
                     if(msg == 'err'){
                         alert('Some problem occured, please try again.');
                     }else{
                         $('#'+target).html(msg);
                     }
                 }
             });
         }
      </script>
   </body>
</html>