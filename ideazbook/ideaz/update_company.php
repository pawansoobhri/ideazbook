<?PHP
   require_once("./include_company/membersite_config.php");
   
   if(!$fgmembersite->CheckLogin())
   {
       $fgmembersite->RedirectToURL("login_company.php");
       exit;
   }
   
   if(isset($_POST['submitted']))
   {
      if($fgmembersite->ChangePassword())
      {
           $fgmembersite->RedirectToURL("update_company.php");
      }
   }
   
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Company Update</title>
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="assets/css/main.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
      <link href="css/landing-page.css" rel="stylesheet">
   </head>
   <body>
      <?php include './htmlMod/companyProfileNav.php';?>
      <div class="container " style="margin-top:30px">
         <h3>Edit Profile</h3>
         <hr>
         <div class="row">
            <div class="col-md-3">
               <div class="text-center">
                  <form action="upload_company_save.php" method="post" enctype="multipart/form-data">
                  <div class="image-container">
            <div class="image-backdrop">
                     <img  src=<?= $fgmembersite->image_path($fgmembersite->UserEmail()); ?> alt="profile pic"   style="margin-top:10px; display:block" onerror="if (this.src != './upload/ib_default.PNG') this.src = './upload/ib_default.PNG';">
                     </div></div>
                     <h6>Upload a different photo...</h6>
                     <input class="form-control" type="file" onchange="this.form.submit()" name="file" >
                     <noscript><input type="submit" name="submit" value="Profile Picture"></noscript>
                  </form>
               </div>
            </div>
            <div class="col-md-9 personal-info">
               <p><B>General info</b></P>
               <form id='changepwd' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
                  <div class="form-group">
                     <input type='hidden' name='submitted' id='submitted' value='1'/>
                     <span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span>
                     <label class="col-lg-3 control-label">Company Name:</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->UserFullName(); ?>" name="nam" readonly>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-lg-3 control-label">Email:</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->UserEmail(); ?>" readonly>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Username:</label>
                     <div class="col-md-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->get_username($fgmembersite->UserEmail()); ?>" readonly>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Original Password:</label>
                     <div class="col-md-8">
                        <input class="form-control" type="password" value="" name='oldpwd'>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">New Password:</label>
                     <div class="col-md-8">
                        <input class="form-control" type="password" value="" name='newpwd'>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label"></label>
                     <div class="col-md-8">
                        <input class="btn btn-primary" type="submit" value="Save Changes" name='Submit'>
                        <span></span>
                        <input class="btn btn-default" type="reset" value="Cancel" >
                     </div>
                  </div>
               </form>
            </div>
            <div class="col-md-9 personal-info">
               <p><B>Company info</b></P>
               <form action="save_info_company.php" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                     <label class="col-lg-3 control-label">CEO Of Company:</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="text" value='<?= $fgmembersite->get_ceo($fgmembersite->UserEmail()); ?>' name='ceo'>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-lg-3 control-label">About(Company's field):</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->get_about($fgmembersite->UserEmail()); ?>" name='about' >
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-lg-3 control-label">Founded:</label>
                     <div class="col-lg-8">
                        <input class="form-control" type="date" placeholder="" value="<?= $fgmembersite->get_founded($fgmembersite->UserEmail()); ?>" name='founded'  >
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Founders:</label>
                     <div class="col-md-8">
                        <input class="form-control" type="text" placeholder="Please seperate the names of respected founders if more than one with a comma" value="<?= $fgmembersite->get_founder($fgmembersite->UserEmail()); ?>" name='founder' >
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Address:</label>
                     <div class="col-md-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->get_address($fgmembersite->UserEmail()); ?>" name='address'>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Contact Details:</label>
                     <div class="col-md-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->get_contact_details($fgmembersite->UserEmail()); ?>" name='contact_details'>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Contact Details:</label>
                     <div class="col-md-8">
                        <input class="form-control" type="text" value="<?= $fgmembersite->get_contact_details($fgmembersite->UserEmail()); ?>" name='contact_details'>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label"></label>
                     <div class="col-md-8">
                        <input class="btn btn-primary" type="submit" value="Save Changes" name='Submit'>
                        <span></span>
                        <input class="btn btn-default" type="reset" value="Cancel" >
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      </div>
   </body>
</html>