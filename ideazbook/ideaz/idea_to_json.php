<?php
require_once("./include/membersite_config.php");
if (!$fgmembersite->CheckLogin()) {
    $fgmembersite->RedirectToURL("login.php");
    exit;
}
//$myfile = './upload/ideaGiver/' . $fgmembersite->get_iduser($fgmembersite->UserEmail()) . "/" . "idea/" . $fgmembersite->get_iduser($fgmembersite->UserEmail()) . '.txt';
$directoryName = "./upload/ideaGiver/" . $fgmembersite->get_iduser($fgmembersite->UserEmail())."/"."idea/";
static $i = 0;
if (isset($_POST['idea'])) {
    if (empty($_POST['idea'])) {
        echo 'All fields are required';
    } else {
        $data = $_POST['idea'];
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0755, true);
            if (strlen($data) > 100) {
                $formdata = array(
                    'data' => $_POST['idea']
                );
                
                $jsondata = json_encode($formdata, JSON_PRETTY_PRINT);
                
                if (file_put_contents($directoryName . $fgmembersite->get_iduser($fgmembersite->UserEmail()).'.json', $jsondata, FILE_APPEND | LOCK_EX))
                   header("Location: profile.php");
                else
                    echo 'Unable to save data';
            } else {
                header("Location: update.php");
                /* Redirect browser */
                exit;
            }
        }
        
        else {
            if (strlen($data) > 100) {
                $formdata = array(
                    'data' => $_POST['idea']
                );
                
                $jsondata = json_encode($formdata, JSON_PRETTY_PRINT);
                
                if (file_put_contents($directoryName . $fgmembersite->get_iduser($fgmembersite->UserEmail()).'.json', $jsondata.',', FILE_APPEND | LOCK_EX))
                    header("Location: profile.php");
                else
                    echo 'Unable to save data';
            } else {
                
                header("Location: update.php");
                /* Redirect browser */
                exit;
            }
            
        }
        
    }
} else
    echo 'Form fields not submited';
?>


