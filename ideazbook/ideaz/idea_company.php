<?PHP
require_once("./include_company/membersite_config.php");

if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("login_company.php");
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Seek Ideas</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="css/landing-page.css" rel="stylesheet">


</head>
<body>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="profile_company.php">iDEAZBOOK</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li></li>
                    <li><a href="logout_company.php">Log Out</a></li>
                    <li><a href="#contact">Help</a> 
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
        <br>
    <br>
    <br>
    <hr>
    
    <div class="col-md-9" id="divMain">

    <h1>Seek Ideas</h1>
    <!--<h3 style="color:#FF6633;"><?php echo $_GET[msg];?></h3>-->
   <!-- *<h3 style="color:#FF6633;"><?php echo $_GET[msg];?></h3>-->
    <hr>
    	                                                
    <form name="enq" method="post" action="email/index_company_idea.php" onsubmit="return validation();">
    
    <fieldset>    <h3>Category Of Idea Required</h3>
    <input type="text" name="name" id="name" value="" class="input-block-level form-control" placeholder="Category" />


<br>
     <h3>Requirements Of Idea</h3>
    <textarea rows="11" name="message" id="message" class="input-block-level form-control" placeholder="Requirements"></textarea>
    <div class="actions">
<br>
    <input type="submit" value="Submit" name="submit" id="submitButton" class="btn btn-info pull    -right" title="Click here to submit your message!" />
    </div>	
    </fieldset>
	
    </form>  				 
    <!--End Contact form -->											 
</div>
    </body>
    </html>