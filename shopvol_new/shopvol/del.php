<!DOCTYPE html>
<html lang="en">
   <?php include("./head.php"); ?>
   <body style="overflow:none">
      <div id="wrapper">
         <div id="header">
            <?php include 'nav.php';?>
         </div>
         <div id="content">
            <div class="container">
               <div class="row" style="padding-top:107px">
                  <div class="col-md-9">
                     <form action="del1.php" accept="image/gif, image/jpeg" method="post" enctype="multipart/form-data">
                        <table>
                           <tbody>
                              <tr>
                                 <td>Name&nbsp;&nbsp;&nbsp;</td>
                                 <td>
                                    <input type="text" placeholder="eg. Digital Electronics" class="form-control" name="product_name">
                                    <p></p>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Product Code&nbsp;&nbsp;&nbsp;</td>
                                 <td>
                                    <input type="text" placeholder="eg. Digital Electronics" class="form-control" name="product_code">
                                    <p></p>
                                 </td>
                              </tr>
                              <tr>
                                 <td></td>
                                 <td><input type="submit" class="btn1 btn1-primary pull-right"></td>
                              </tr>
                           </tbody>
                        </table>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div id="footer">   
            <?php include("./footer.html"); ?>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>