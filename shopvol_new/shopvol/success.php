<!DOCTYPE html>
<html lang="en">
<?php include("./head.php"); ?>
   <body style="overflow:none">
      <div id="wrapper">
         <div id="header">
            <?php include 'nav.php';?>
         </div>
         <div id="content">
            <div class="container">
               <div class="row" style="margin:150px 0px">
                  <div class="col-md-9">
                     <span style="font-size: 1.65em;">Your Profile has been Updated!!!</span>
                  </div>
               </div>
            </div>
         </div>
         
         <div id="footer">   
            <?php include("./footer.html"); ?>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>