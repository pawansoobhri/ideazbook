<?php include 'init_direct.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ShopVol | The largest online book store</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/pawan.css" rel="stylesheet">
    <link href="css/shop-homepage.css" rel="stylesheet">
    <link href="style/style.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/png" href="images/favi.png">

    <script>
    function drag_start(event) 
    {
    var style = window.getComputedStyle(event.target, null);
    var str = (parseInt(style.getPropertyValue("left")) - event.clientX) + ',' + (parseInt(style.getPropertyValue("top")) - event.clientY)+ ',' + event.target.id;
    event.dataTransfer.setData("Text",str);
    } 

    function drop(event) 
    {
    var offset = event.dataTransfer.getData("Text").split(',');
    var dm = document.getElementById(offset[2]);
    dm.style.left = (event.clientX + parseInt(offset[0],10)) + 'px';
    dm.style.top = (event.clientY + parseInt(offset[1],10)) + 'px';
    event.preventDefault();
    return false;
    }

    function drag_over(event)
    {
    event.preventDefault();
    return false;
    }   
    </script>
</head>
<body style="overflow:none; background-image: url('http://www.shopvol.com/images/contact-us.jpg'); background-repeat: repeat;background-position: top center;  background-attachment: scroll;" >
       <div><?php include 'nav.php';?></div>

      <div class="container">
        <div class="row" style="padding-top:107px">
           <div class="col-md-12">
<div class="section-header" style="text-align: center; padding-bottom: 10px;">
<h2 class="white-text" style="padding-bottom: 10px;line-height: 40px;position: relative;display: inline-block;font-size: 45px;text-transform: uppercase;margin-top: 15px;margin-bottom: 0;color: #ffffff;">Sorry! Didn't matched you..</h2><div class="white-text section-legend">Below is something special for you..</div></div>



             
<!-- View Cart Box Start -->
<?php
if(isset($_SESSION["cart_products"]) && count($_SESSION["cart_products"])>0)
{
	echo '<div class="cart-view-table-front" id="view-cart" id="view-cart" id="dragg" draggable="true" ondragstart="drag_start(event)">';
	echo '<center><h4>Cart</h4></center>';
	echo '<form method="post" action="cart_update.php">';
	echo '<table width="100%"  cellpadding="6" cellspacing="0">';
	echo '<tbody>';

	$total =0;
	$b = 0;
	foreach ($_SESSION["cart_products"] as $cart_itm)
	{
		$product_name = $cart_itm["product_name"];
		$product_qty = $cart_itm["product_qty"];
		$product_price = $cart_itm["product_price"];
		$product_code = $cart_itm["product_code"];
		$product_color = $cart_itm["product_color"];
		$bg_color = ($b++%2==1) ? 'odd' : 'even'; //zebra stripe
		echo '<tr class="'.$bg_color.'">';
		echo '<td>Qty <input type="text" size="2" maxlength="2" name="product_qty['.$product_code.']" value="'.$product_qty.'" /></td>';
		echo '<td>'.$product_name.'</td>';
		echo '<td><input type="checkbox" name="remove_code[]" value="'.$product_code.'" /> Remove</td>';
		echo '</tr>';
		$subtotal = ($product_price * $product_qty);
		$total = ($total + $subtotal);
	}
	echo '<td colspan="4">';
	echo '<button type="submit" class="btn1 btn1-primary pull-left">Update</button><a class="btn1 btn1-primary pull-right" href="view_cart.php" class="button">Checkout</a>';
	echo '</td>';
	echo '</tbody>';
	echo '</table>';
	
	$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	echo '<input type="hidden" name="return_url" value="'.$current_url.'" />';
	echo '</form>';
	echo '</div>';

}
?>
<!-- View Cart Box End -->
<div class="container1" >
<?php
$results = $mysqli->query("SELECT id,product_code, product_name,product_det,category, product_desc,product_edi, product_img_name, price,photo,mrp FROM products  WHERE product_name like '%".$ss."%'");
if($results){ 
$products_item = '';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$e=$obj->category;
$a=substr($e,6);
if($a=="old")
$a="Old";
$products_item .= <<<EOT
<div class="container" >
<div class="jumbotron1" style="background-color: #fff;  -webkit-box-shadow: 0 0 2px 0 #d3cbb8; -moz-box-shadow: 0 0 2px 0 #d3cbb8; box-shadow: 0 0 2px 0 #d3cbb8; margin: 0 auto;">
<div class="row" style="padding-bottom:19px">
<div class="col-sm-6 col-md-4 col-lg-4">
<img src="showimg.php?as=$obj->product_code" style="margin: 19px 49px auto; width:131px; height:158px">           
</div>
<div class="col-sm-6 col-md-4 col-lg-8">
<u><h3 onClick="location.href='display.php?product_code=$obj->product_code'">$obj->product_name</h3></u>
<div style="border-top: 1px solid #f2f2f2;padding: 0px 0;margin-bottom: 10px;">
<div style="width: 151px; float: left; overflow: hidden;     margin-top: 10px;">
<span style="font-family: Museo,Helvetica,arial,san-serif; font-weight: normal; font-size: 18px; color: #565656;">&#8377;&nbsp;{$obj->price}</span>
<br>
<strike><span style="font-family: Museo,Helvetica,arial,san-serif; font-weight: normal;color: #565656;">&#8377;&nbsp;{$obj->mrp}</span></strike>
</div>
</div>
<div style="color: #848484;font-size: 9px;">
<span>
Author: {$obj->product_desc}
<br>
Edition: {$obj->product_edi} 
<br>
Status: {$a} 
</span>
</div>
<div class="clearfix visible-sm-block"></div>
<form method="post" action="cart_update.php">
<div style="padding: 15px 0 0 0;border-top: 0;">
<div style=" float: left;margin-left: -6px; ">
<div style="margin-top: 7px;">
<input type="hidden" name="product_code" value="{$obj->product_code}" />
<input type="hidden" name="type" value="add" />
<input type="hidden" name="return_url" value="{$current_url}" />
<button type="submit" class="btn1 btn1-primary pull-left">Add to Cart</button>
</div>
<!---<div style="padding: 0px 20px 4px 109px;"><button type="submit" class="btn1 btn1-primary">Click & Buy</button></div>--->
</div>
<div style=" float: left; /*border-left: 1px solid #d3d3d3;*/ flex-grow: 1; position: relative; padding-top: 7px; position: relative; padding-left: 0px;margin-left: 47px;">
<b>Qt.</b>
<input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

EOT;
}
$products_item .= '';
echo $products_item;
}
?> 
</div>
</div>  
</div>
<?php include("./footer.html"); ?>
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
</body>
</html>