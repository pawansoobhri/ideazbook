/*******LOB JSON Format*******/
/*var track_params = {"org":"DEL","dest":"BOM"};*/

/*******LOB Function**********/
/*dropoff.track('Flights','Confirm',track_params);*/

var dropoff = {
	enabled: {
		dom2:true,
		int2:true,
		mdomwap:true,
		mintwap:true

	},
	track: function(params){
		this.pushData(params);
	},
	pushData: function(ajaxparams){
		$.ajax({
                type: "POST",
                url: "/dropoff/dropoff_monitor",
                data: {
                 'trackingData':JSON.stringify(ajaxparams)
                }
            });
	},
	pushRaw:function(rawJson) {
		$.ajax({
                type: "POST",
                url: "/dropoff/dropoff_monitor",
                data: {
                 'trackingData':rawJson
                }
        });
	}
};