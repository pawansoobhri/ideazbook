<?php
   session_start();
   include_once("config.php");
   $name=$_POST["name"];
   $phone=$_POST["phone"];
   $email=$_POST["email"];
   $address=$_POST["address"];
   ?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>View shopping cart</title>
      <link href="style/style.css" rel="stylesheet" type="text/css">
   </head>
   <body>
      <h1 align="center">View Cart</h1>
      <div class="cart-view-table-back">
         <form method="post" action="cart_update.php">
            <table width="100%"  cellpadding="6" cellspacing="0">
               <thead>
                  <tr>
                     <th>Quantity</th>
                     <th>Name</th>
                     <th>Price</th>
                     <th>Total</th>
                     <th>Remove</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     $ma='';
                     	if(isset($_SESSION["cart_products"])) //check session var
                         {
                     		$total = 0; //set initial total value
                     		$b = 0; //var for zebra stripe table 
                     		foreach ($_SESSION["cart_products"] as $cart_itm)
                             {
                     			//set variables to use in content below
                     			$product_name = $cart_itm["product_name"];
                     			$product_qty = $cart_itm["product_qty"];
                     			$product_price = $cart_itm["product_price"];
                     			$product_code = $cart_itm["product_code"];
                     			$product_color = $cart_itm["product_color"];
                     			$subtotal = ($product_price * $product_qty);
                     
                     		   	$bg_color = ($b++%2==1) ? 'odd' : 'even'; //class for zebra stripe 
                     		    echo '<tr class="'.$bg_color.'">';
                     $ma .='			<td><input type="text" size="2" maxlength="2" name="product_qty['.$product_code.']" value="'.$product_qty.'" /></td>';
                     $ma .='			<td>'.$product_name.'</td>';
                     $ma .='			<td>'.$currency.$product_price.'</td>';
                     $ma .='			<td>'.$currency.$subtotal.'</td>';
                     $ma .=' </tr>';
                     
                                 echo '</tr>';
                     			$total = ($total + $subtotal); //add subtotal to total var
                             }
                     		$grand_total = $total + $shipping_cost; //grand total including shipping cost
                     		$shipping_cost = ($shipping_cost)?'Shipping Cost : '.$currency. sprintf("%01.2f", $shipping_cost).'<br />':'';
                     	}
                     
                         ?>
                  <tr>
                     <td colspan="5"><span style="float:right;text-align: right;"><?php echo $shipping_cost ?>Amount Payable : <?php echo sprintf("%01.2f", $grand_total);?></span></td>
                  </tr>
                  <tr></tr>
               </tbody>
            </table>
            <input type="hidden" name="return_url" value="<?php 
               $current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
               echo $current_url; ?>" />
         </form>
         <h1>Order Placed</h1>
      </div>
   </body>
</html>