<?php include 'init_direct.php';?>
    <!DOCTYPE html>
<html lang="en">
<?php include("./head.php"); ?>
   <body style="overflow:none">
      <div id="wrapper">
         <div id="header">
            <?php include 'nav.php';?>
         </div>
         <div id="content">
            <div class="container">
               <div class="row" style="margin:150px 0px">
                  <div class="col-md-6">
                      <h4>Register</h4>
                  <?php
                  if(isset($_SESSION["showmydiv"])){
                      if($_SESSION['showmydiv']=='block'){
                        echo '<div style="display:none;display:'.$_SESSION['showmydiv'].';margin-bottom: 25px;"><span style="color: #ff5d00;">User Already Exists</span></div>';
                      }
                  }
                  unset($_SESSION['showmydiv']);
                    ?>
                     <form action="adduser.php" accept="image/gif, image/jpeg" method="post" enctype="multipart/form-data">
                        <table>
                           <tr>
                              <td>First Name&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <input type="text" placeholder="Jack" class="form-control" name="fname" required/>
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Last Name:</td>
                              <td>
                                 <input type="text" placeholder="Sparrow" class="form-control" name="lname" required />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Address&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <input type="text" placeholder="123, Streen No:" class="form-control" name="address" required/>
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>City&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <input type="text" placeholder="Mumbai" class="form-control" name="city" required />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Pin Code&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <input type="number" placeholder="144602" class="form-control" name="pin" required/>
                                 <p></p>
                              </td>
                           </tr>                          
                           <tr>
                              <td>E-Mail&nbsp;&nbsp;&nbsp; </td>
                              <td>
                                 <input class="form-control" type="email" placeholder="apc@xyz.com;" name="email" required/>
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Password&nbsp;&nbsp;&nbsp; </td>
                              <td>
                                 <input class="form-control" type="password" name="pwd" required />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td><input type="submit" value="Register" class="btn1 btn1-primary pull-right" /></td>
                              <td><input type="reset"  value="Reset" class="btn1 btn1-primary pull-right"></td>
                           </tr>
                        </table>
                     </form>
                  </div>


               </div>
            </div>
         </div>
         <div id="footer">   
            <?php include("./footer.html"); ?>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>