<?php include 'init_direct.php';?>

<!-------------------------------------------------------------------------------------------------------------------------------------------->
<!DOCTYPE html>
<html>
<?php include("./head.php"); ?>
<!-------------------------------------------------------------------------------------------------------------------------------------------->

<script>  
function checkPhone()
        {
            var x = document.myform.phone.value;
            
           if(isNaN(x)||x.indexOf(" ")!=-1)
           {
              document.getElementById("phoneerr").innerHTML="Enter numeric value";
              return false; 
           }
           
         
           if (!(x.length==10))
           {
                document.getElementById("phoneerr").innerHTML="enter 10 characters";
                return false;
           }
           
        }

function ValidateEmail(mail)   
{  
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myform.email.value))  
  {     document.getElementById("emailerr").innerHTML="";
    return (true)  
  }  
   document.getElementById("emailerr").innerHTML="Invalid Email!!";
    return (false)  
} 
function validateform(){  
var name=document.myform.name.value;  
var password=document.myform.address.value;  
  
if (name==null || name==""){  
  document.getElementById("nameerr").innerHTML="Invalid Name!";
  return false;  
}else
document.getElementById("nameerr").innerHTML="";

 if(password.length<6){  
  document.getElementById("addresserr").innerHTML="Invalid Address!";
  return false;  
  }  
else
document.getElementById("addresserr").innerHTML="";
 if(!(ValidateEmail(document.myform.email.value)))
return false;
return checkPhone();
}  

</script>  
<body>
<?php include 'nav.php';?>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
        <div class="row" style="padding-top:107px">
<h1 align="center">View Cart</h1>
<div class="cart-view-table-back">
<form method="post" action="cart_update.php">
<table width="100%"  cellpadding="6" cellspacing="0"><thead><tr><th>Quantity</th><th>Name</th><th>Price</th><th>Total</th><th>Remove</th></tr></thead>
  <tbody>
 	<?php
	if(isset($_SESSION["cart_products"])) //check session var
    {
		$total = 0; //set initial total value
		$b = 0; //var for zebra stripe table 
		foreach ($_SESSION["cart_products"] as $cart_itm)
        {
			//set variables to use in content below
			$product_name = $cart_itm["product_name"];
			$product_qty = $cart_itm["product_qty"];
			$product_price = $cart_itm["product_price"];
			$product_code = $cart_itm["product_code"];
			$product_color = $cart_itm["product_color"];
			$subtotal = ($product_price * $product_qty); //calculate Price x Qty
			
		   	$bg_color = ($b++%2==1) ? 'odd' : 'even'; //class for zebra stripe 
		    echo '<tr class="'.$bg_color.'">';
			echo '<td><input type="text" size="2" maxlength="2" name="product_qty['.$product_code.']" value="'.$product_qty.'" /></td>';
			echo '<td>'.$product_name.'</td>';
			echo '<td>'.$currency.$product_price.'</td>';
			echo '<td>'.$currency.$subtotal.'</td>';
			echo '<td><input type="checkbox" name="remove_code[]" value="'.$product_code.'" /></td>';
            echo '</tr>';
			$total = ($total + $subtotal); //add subtotal to total var
        }
		
		$grand_total = $total + $shipping_cost; //grand total including shipping cost
		
		
		$shipping_cost = ($shipping_cost)?'Shipping Cost : '.$currency. sprintf("%01.2f", $shipping_cost).'<br />':'';
	}
    ?>
    <tr><td colspan="5"><span style="float:right;text-align: right;"><?php echo $shipping_cost; ?>Amount Payable : <?php echo sprintf("%01.2f", $grand_total);?></span></td></tr>
    <tr><td colspan="5"><a href="index.php" class="btn1 btn1-primary pull-right">Add More Items</a><button class="btn1 btn1-primary pull-right" type="submit">Update</button></td></tr>
  </tbody>
</table>
<input type="hidden" name="return_url" value="<?php 
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
echo $current_url; ?>" />
</form>
<!-------------------------------------------------------------------------------------------------------------------------------------------->

<table>
<form method="post" action="ex.php" name="myform" onsubmit="return validateform()">
<tr><td>Name:</td> <td><input type="text" name="name" /><span id="nameerr"></td></tr>
<tr><td>Address: </td> <td><input type="text" name="address" /><span id="addresserr"></td></tr>
<tr><td>Email: </td> <td><input type="text" name="email" /><span id="emailerr"></td></tr>
<tr><td>Phone No.</td> <td><input type="text" name="phone" /></td></tr><tr><td><p></p><span id="phoneerr"></td></tr>
<tr><td>Book Name.</td> <td><input type="text" name="book_name" /></td></tr><tr><td><p></p><span id="phoneerr"></td></tr>
<tr><td>Book Author.</td> <td><input type="text" name="book_author" /></td></tr><tr><td><p></p><span id="phoneerr"></td></tr>
<tr><td>Book Price.</td> <td><input type="text" name="book_price" /></td></tr><tr><td><p></p><span id="phoneerr"></td></tr>
<tr><td>Book Pic.</td> <td>
<?php
if(isset($_REQUEST['submit']))
{
  $filename=  $_FILES["imgfile"]["name"];
  if ((($_FILES["imgfile"]["type"] == "image/gif")|| ($_FILES["imgfile"]["type"] == "image/jpeg") || ($_FILES["imgfile"]["type"] == "image/png")  || ($_FILES["imgfile"]["type"] == "image/pjpeg")) && ($_FILES["imgfile"]["size"] < 200000))
  {
    if(file_exists($_FILES["imgfile"]["name"]))
    {
      echo "File name exists.";
    }
    else
    {
      move_uploaded_file($_FILES["imgfile"]["tmp_name"],"uploads/$filename");
      echo "Upload Successful . <a href='uploads/$filename'>Click here</a> to view the uploaded image";
    }
  }
  else
  {
    echo "invalid file.";
  }
}
else
{
?>
<form method="post" enctype="multipart/form-data">
File name:<input type="file" name="imgfile"><br>
<input type="submit" name="submit" value="upload">

</form>
<?php
}
?> </td></tr>
<tr><td></td><td><input type="submit" class="btn1 btn1-primary pull-right"/></td></tr>
</form></table>
</div>

</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
</body>
</html>