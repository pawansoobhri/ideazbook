<?php
if (session_id() == '' || !isset($_SESSION)) {
    session_start();
}
include_once("config.php");
include_once("sendmail.php");
$name        = $_POST["name"];
$phone       = $_POST["phone"];
$email       = $_POST["email"];
$address     = $_POST["address"];
$product_kau = "";
$temp        = 1;

?>
<!DOCTYPE html>
<html>
  <?php
include("./head.php");
?>
  <body>
    <?php
include 'nav.php';
?>
    <div class="row" style="padding-top:107px">
      <h1 align="center">Order Placed</h1>
      <div class="cart-view-table-back">
         <form method="post" action="cart_update.php">
            <table width="100%"  cellpadding="6" cellspacing="0">
               <thead>
                  <tr>
                     <th>Quantity</th>
                     <th>Name</th>
                     <th>Price</th>
                     <th>Total</th>
                     <th>Remove</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
if (isset($_SESSION["cart_products"])) //check session var
    {
    $total = 0; //set initial total value
    $b     = 0; //var for zebra stripe table 
    foreach ($_SESSION["cart_products"] as $cart_itm) {
        $temp          = 2;
        //set variables to use in content below
        $product_name  = $cart_itm["product_name"];
        $product_qty   = $cart_itm["product_qty"];
        $product_price = $cart_itm["product_price"];
        $product_code  = $cart_itm["product_code"];
        $subtotal      = ($product_price * $product_qty);
        
        $mysqli->query("insert into orders (product_code,product_name,name,address,email,qty,phone,price,subtotal) value('$product_code','$product_name','$name','$address','$email','$product_qty','$phone','$product_price','$subtotal')");
        unset($_SESSION["cart_products"][$product_code]);
        $bg_color = ($b++ % 2 == 1) ? 'odd' : 'even'; //class for zebra stripe 
        echo '<tr class="' . $bg_color . '">';
        echo '<td><input type="text" size="2" maxlength="2" name="product_qty[' . $product_code . ']" value="' . $product_qty . '" /></td>';
        echo '<td>' . $product_name . '</td>';
        $product_kau .= $product_name . "  ";
        echo '<td>' . $currency . $product_price . '</td>';
        echo '<td>' . $currency . $subtotal . '</td>';
        echo '<td><input type="checkbox" name="remove_code[]" value="' . $product_code . '" /></td>';
        echo '</tr>';
        $total = ($total + $subtotal); //add subtotal to total var
    }
    
    $grand_total = $total + $shipping_cost; //grand total including shipping cost
    
    
    $shipping_cost = ($shipping_cost) ? 'Shipping Cost : ' . $currency . sprintf("%01.2f", $shipping_cost) . '<br />' : '';
}
?>
                 <tr>
                     <td colspan="5"><span style="float:right;text-align: right;"><?php
echo $shipping_cost;
?>Amount Payable : <?php
echo sprintf("%01.2f", $grand_total);
?></span></td>
                  </tr>
                  <tr></tr>
               </tbody>
            </table>
            <input type="hidden" name="return_url" value="<?php
$current_url = urlencode($url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
echo $current_url;
?>" />
         </form>
         <h1>Order Placed</h1>
      </div>
      </div>
      <?php
if ($temp == 2) {
    $m = new Mail; // create the mail
    $m->From("Shop Vol");
    $m->To($email);
    $m->Subject("Thanks for Placing Order");
    
    $message = $product_kau;
    $m->Body($message); // set the body
    $m->Cc("someone@somewhere.fr");
    $m->Bcc("pawanrockz93@gmail.com");
    //  $m->Priority(4) ;        // set the priority to Low
    //  $m->Attach( "/home/leo/toto.gif", "image/gif" ) ;        // attach a file of type image/gif
    
    //alternatively u can get the attachment uploaded from a form
    //and retreive the filename and filetype and pass it to attach methos
    
    $m->Send(); // send the mail
}


?>
        <?php

?>
  </body>
</html>