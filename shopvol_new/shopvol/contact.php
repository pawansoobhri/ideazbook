<?php include 'init_direct.php';?>
<!DOCTYPE html>
<html lang="en">
   <?php include("./head.php"); ?>
   <body style="overflow:none;">
      <div><?php include 'nav.php';?></div>
      <div class="container">
         <div class="row" style="padding-top:107px">
            <div class="col-md-12">
               <div class="section-header" style="text-align: center; padding-bottom: 10px;">
                  <h2 class="white-text" style="padding-bottom: 10px;line-height: 40px;position: relative;display: inline-block;font-size: 45px;text-transform: uppercase;margin-top: 15px;margin-bottom: 0;color: #ffffff;">Contact Us</h2>
                  <div class="white-text section-legend">We are there to answer.</div>
               </div>
               <section>
                  <form  name="contactForm" id='contact_form' method="post" action='php/email.php'>
                     <input type="hidden" name="action" value="submit"> 
                     <div class="col-lg-4 col-sm-4"  data-sr-init="true" data-sr-complete="true">
                        <label for="name" style="clip: rect(1px, 1px, 1px, 1px); position: absolute !important;">Your Name</label>
                        <input required="required" type="text" name="name" id="name" placeholder="Your Name" class="form-control input-box" style="width: 100%; margin: auto; margin-bottom: 20px; border-radius: 4px;height: 50px;" value="">
                     </div>
                     <div class="col-lg-4 col-sm-4"  data-sr-init="true" data-sr-complete="true">
                        <label for="email" class="screen-reader-text" style="clip: rect(1px, 1px, 1px, 1px); position: absolute !important;">Your Email</label>
                        <input required="required" type="email" name="email" id="email" placeholder="Your Email" class="form-control input-box" value="" style="width: 100%; margin: auto; margin-bottom: 20px; border-radius: 4px;height: 50px;">
                     </div>
                     <div class="col-lg-4 col-sm-4"  data-sr-init="true" data-sr-complete="true">
                        <label for="subject" class="screen-reader-text" style="clip: rect(1px, 1px, 1px, 1px); position: absolute !important;">Subject</label>
                        <input required="required" type="text" name="subject" id="subject" placeholder="Subject" class="form-control input-box" value="" style="width: 100%; margin: auto; margin-bottom: 20px; border-radius: 4px;    height: 50px;">
                     </div>
                     <div class="col-lg-12 col-sm-12" data-scrollreveal="enter right after 0s over 1s" data-sr-init="true" data-sr-complete="true">
                        <label for="message" class="screen-reader-text" class="screen-reader-text" style="clip: rect(1px, 1px, 1px, 1px); position: absolute !important;">Your Message</label>
                        <textarea name="message" id="message" class="form-control textarea-box" placeholder="Your Message" style="text-align: left; text-transform: none; padding: 9px; min-height: 250px; padding-left: 15px; display: inline-block; border-radius: 4px; background: rgba(255,255,255, 0.95);"></textarea>
                     </div>
                     <button class="btn btn-primary custom-button red-btn" type="submit" data-sr-init="true" data-sr-complete="true" style="float: right; margin-right: 15px; color: #FFF !important; -webkit-transition: all 0.3s ease-in-out; transition: all 0.3s ease-in-out; background: #549E01; display: inline-block !important; text-align: center; text-transform: uppercase; padding: 13px 35px 13px 35px; border-radius: 4px; margin: 10px; border: none;" id='send_message' value="send" name="send">
                        Submit
                        <div class="g-recaptcha" value="Send email"/></div>
                     </button>
                  </form>
               </section>
            </div>
         </div>
      </div>
      <?php include("./footer.html"); ?>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>