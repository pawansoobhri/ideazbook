<?php include 'init_direct.php';?>
<?php
$d=$_GET["product_code"];
$e="NULL";
?>
<!DOCTYPE html>
<html lang="en">
<?php include("./head.php"); ?>
<body style="overflow:none" ondragover="drag_over(event)" ondrop="drop(event)">
       <?php include 'nav.php';?>
<?php
if(isset($_SESSION["cart_products"]) && count($_SESSION["cart_products"])>0)
{
	echo '<div class="cart-view-table-front" id="view-cart" id="view-cart" id="dragg" draggable="true" ondragstart="drag_start(event)">';
	echo '<center><h4>Cart</h4></center>';
	echo '<form method="post" action="cart_update.php">';
	echo '<table width="100%"  cellpadding="6" cellspacing="0">';
	echo '<tbody>';
	$total =0;
	$b = 0;
	foreach ($_SESSION["cart_products"] as $cart_itm)
	{
		$product_name = $cart_itm["product_name"];
		$product_qty = $cart_itm["product_qty"];
		$product_price = $cart_itm["product_price"];
		$product_code = $cart_itm["product_code"];
		
		$bg_color = ($b++%2==1) ? 'odd' : 'even'; //zebra stripe
		echo '<tr class="'.$bg_color.'">';
		echo '<td>Qty <input type="text" size="2" maxlength="2" name="product_qty['.$product_code.']" value="'.$product_qty.'" /></td>';
		echo '<td>'.$product_name.'</td>';
		echo '<td><input type="checkbox" name="remove_code[]" value="'.$product_code.'" /> Remove</td>';
		echo '</tr>';
		$subtotal = ($product_price * $product_qty);
		$total = ($total + $subtotal);
	}
	echo '<td colspan="4">';
	echo '<button type="submit" class="btn1 btn1-primary pull-left">Update</button><a class="btn1 btn1-primary pull-right" href="view_cart.php" class="button">Checkout</a>';
	echo '</td>';
	echo '</tbody>';
	echo '</table>';
	$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	echo '<input type="hidden" name="return_url" value="'.$current_url.'" />';
	echo '</form>';
	echo '</div>';
}
?>
<?php
$results = $mysqli->query("SELECT id,product_code, product_name,product_det,category, product_desc,product_edi, product_img_name, price,photo,mrp FROM products  WHERE product_code='$d'");
if($results){ 
$products_item = '';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$e=$obj->category;
$a=substr($e,6);
if($a=="old")
$a="Old";

$products_item .= <<<EOT
<div class="container" >
<div class="jumbotron" style="background-color: #fff;  -webkit-box-shadow: 0 0 2px 0 #d3cbb8; -moz-box-shadow: 0 0 2px 0 #d3cbb8; box-shadow: 0 0 2px 0 #d3cbb8; margin: 0 auto;">
<div class="row">
<div class="col-sm-6 col-md-4 col-lg-4">
<img src="showimg.php?as=$obj->product_code" style="margin: 92px 49px auto; width:200px; height:250px">           
</div>
<div class="col-sm-6 col-md-4 col-lg-8">
<h1>$obj->product_name</h1>
<div style="border-top: 1px solid #f2f2f2;padding: 0px 0;margin-bottom: 10px;">
<div style="width: 211px; float: left; overflow: hidden;     margin-top: 10px;">
<span style="font-family: Museo,Helvetica,arial,san-serif; font-weight: normal; font-size: 28px; color: #565656;">&#8377;&nbsp;{$obj->price}</span>
<br>
<strike><span style="font-family: Museo,Helvetica,arial,san-serif; font-weight: normal;color: #565656;">&#8377;&nbsp;{$obj->mrp}</span></strike>
</div>
</div>
<div style="color: #848484;font-size: 11px;">
<span>
Author: {$obj->product_desc}
<br>
Edition: {$obj->product_edi} 
<br>
Status: {$a} 
</span>
</div>
<div class="clearfix visible-sm-block"></div>
<form method="post" action="cart_update.php">
<div style="padding: 15px 0 0 0;border-top: 0;">
<div style=" float: left;margin-left: 12px; ">
<div style="margin-top: 7px;">
<input type="hidden" name="product_code" value="{$obj->product_code}" />
<input type="hidden" name="type" value="add" />
<input type="hidden" name="return_url" value="{$current_url}" />
<button type="submit" class="btn1 btn1-primary pull-left">Add to Cart</button>
</div>
<!---<div style="padding: 0px 20px 4px 109px;"><button type="submit" class="btn1 btn1-primary">Click & Buy</button></div>--->
</div>
<div style=" float: left; /*border-left: 1px solid #d3d3d3;*/ flex-grow: 1; position: relative; padding-top: 7px; position: relative; padding-left: 0px;margin-left: 93px;">
<b>Qt.</b>
<input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
</div>
</div>
</form>
<div style="color: #848484;font-size: 11px;padding-top: 60px;">
<span>
<pre style="background-color:ffffff;">{$obj->product_det}</pre>
</span>
</div>
</div>
</div>
</div>
EOT;
}
$products_item .= '';
echo $products_item;
}
?> 
</body>
<div class="col-md-12">
<?php
$results = $mysqli->query("SELECT id,product_code, product_name, product_desc, product_img_name, price,photo,category FROM products  WHERE category='$e'  ORDER BY RAND() LIMIT 6");
if($results){ 
$products_item = '<ul class="products1">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
<li class="product">
<form method="post" action="cart_update.php">
<div class="thumbnail2  box">
<div class="product-content">
<div class="product-thumb" >
<div  class="imgWrap" onClick="location.href='display.php?product_code=$obj->product_code'">
<div class="img"><img src="showimg.php?as=$obj->product_code" width="42" height="175"></div>
<div class="box1 imgDescription" style="padding:6px" >
<div style="white-space: nowrap;text-overflow: ellipsis;" class="product-desc2" >{$obj->product_name}</div>
<div style="white-space: nowrap;text-overflow: ellipsis;" class="product-desc1" >{$obj->product_desc}</div>
<span>{$currency}{$obj->price}
Qt.
<input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
<input type="hidden" name="product_code" value="{$obj->product_code}" />
<input type="hidden" name="type" value="add" />
<input type="hidden" name="return_url" value="{$current_url}" />
<p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div></div></div></div></div>
</form>
</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<?php include("./footer.html"); ?>
</html>            		