<?php
session_start();
include_once("config.php");
//current URL of the Page. cart_update.php redirects back to this URL
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
?>
<!------------------------------------------------------------------------------------------------------------------->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ShopZip</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
<!------------------------------------------------------------------------------------------------------------------->
<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">ShopZip</a>
            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#">SignUp</a>
                    </li>
                    <li>
                        <a href="#">Login</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
<?php include 'nav.html';?>
    <!-- Page Content -->
    <div class="container">

        <div class="row" style="padding-top:40px">

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">
<!--------------------------------------------------------------------------------------------------------------------------------------------
                    <div class="city">
                        <div class="thumbnail">
                            <img src="./img/pen.png" alt="">
                            <div class="caption">
                                <h4 class="pull-right">&#8377; 25</h4>
                                <h4><a href="#">Linc Pens</a>
                                </h4> 
<p>Blue Ball Pen<a href="#myModal" class="btn btn-primary pull-right" data-toggle="modal">Buy</a></p>                              
                           </div>     
                  </div>
                    </div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
                    <div class="city">
                        <div class="thumbnail">
                           <img src="http://cdn.firstcry.com/brainbees/images/products/zoom/55330a.jpg" alt="">
                            <div class="caption">
                                <h4 class="pull-right">&#8377; 25</h4>
                                <h4><a href="#">Linc Pens</a>
                                </h4>
<p>Blue Ball Pen<a href="#myModal" class="btn btn-primary pull-right" data-toggle="modal">Buy</a></p>
                                 </div> 
                    </div>
                    </div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
                    <div class="city">
                        <div class="thumbnail">
                            <img src="http://stationerybazaar.com/images/Apsara%20Non%20dust%20eraser.jpg" alt="">
                            <div class="caption">
                                <h4 class="pull-right">&#8377; 25</h4>
                                <h4><a href="#">Linc Pens</a>
                                </h4>
<p>Blue Ball Pen<a href="#myModal" class="btn btn-primary pull-right" data-toggle="modal">Buy</a></p>
                       </div> 
                      </div>
                    </div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------------------------------------------------------->
                    <div class="city">
                        <div class="thumbnail">
                            <img src="http://3.imimg.com/data3/IY/NX/MY-696284/images-cx-530x-250x250.png" alt="">
                            <div class="caption">
                                <h4 class="pull-right">&#8377; 25</h4>
                                <h4><a href="#">Linc Pens</a>
                                </h4>
<p>Blue Ball Pen<a href="#myModal" class="btn btn-primary pull-right" data-toggle="modal">Buy</a></p>
                       </div> 
                      </div>
                    </div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
                    <div class="city1">
                        <div class="thumbnail1">
                            <img src="https://mypustak.com/wp-content/uploads/2015/05/Mathematics-Class-10.jpg" alt="">
                            <div class="caption">
                                <h4 class="pull-right">&#8377; 25</h4>
                                <h4><a href="#">Linc Pens</a></h4>
<p>Blue Ball Pen<a href="#myModal" class="btn btn-primary pull-right" data-toggle="modal">Buy</a></p>
                        </div>
                    </div>
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
                  
<!-------------------------------------------------------------------------------------------------------------------------------------->


<!-- Products List Start -->
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  ORDER BY id ASC");
if($results){ 
$products_item = '<div class="city1">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT

	<form method="post" action="cart_update.php">
                    

<!---<h3>{$obj->product_name}</h3>--->
	<div class="thumbnail1"><img height=23px src="images/{$obj->product_img_name}">
<!---	<div class="product-desc">{$obj->product_desc}</div>--->
	<div class="caption">
<h4 class="pull-right">{$currency}{$obj->price} </h4>

	
<!---<label>
	<span>Color</span>
	<select name="product_color">
	<option value="Black">Black</option>
	<option value="Silver">Silver</option>
	</select>
</label>--->
	<p></p>
	<label>
		<span>Qt.</span>
		<input type="text" size="2" maxlength="2" name="product_qty" value="1" />
	</label>
	

	<input type="hidden" name="product_code" value="{$obj->product_code}" />
	<input type="hidden" name="type" value="add" />
	<input type="hidden" name="return_url" value="{$current_url}" />
	<div class="pull-right"><button type="submit" class="btn btn-primary pull-right add_to_cart">Add</button></div>
	</div></div>
	</form>
	
EOT;
}
$products_item .= '</div>';
echo $products_item;
}
?>    
<!-------------------------------------------------------------------------------------------------------------->


<!-- View Cart Box Start -->
<?php
if(isset($_SESSION["cart_products"]) && count($_SESSION["cart_products"])>0)
{
	echo '<div class="table-responsive cart-view-table-front" id="view-cart">';
	echo '<h3>Cart</h3>';
	echo '<form method="post" action="cart_update.php">';
	echo '<table class="table" width="100%"  cellpadding="6" cellspacing="0">';
	echo '<tbody>';
	$total =0;
	$b = 0;
	foreach ($_SESSION["cart_products"] as $cart_itm)
	{
		$product_name = $cart_itm["product_name"];
		$product_qty = $cart_itm["product_qty"];
		$product_price = $cart_itm["product_price"];
		$product_code = $cart_itm["product_code"];
		$product_color = $cart_itm["product_color"];
		$bg_color = ($b++%2==1) ? 'odd' : 'even'; //zebra stripe
		echo '<tr class="'.$bg_color.'">';
		echo '<td>Qty <input type="text" size="2" maxlength="2" name="product_qty['.$product_code.']" value="'.$product_qty.'" /></td>';
		echo '<td>'.$product_name.'</td>';
		echo '<td><input type="checkbox" name="remove_code[]" value="'.$product_code.'" /> Remove</td>';
		echo '</tr>';
		$subtotal = ($product_price * $product_qty);
		$total = ($total + $subtotal);
	}
	echo '<td colspan="4">';
	echo '<button type="submit">Update</button><a href="view_cart.php" class="button">Checkout</a>';
	echo '</td>';
	echo '</tbody>';
	echo '</table>';
	
	$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	echo '<input type="hidden" name="return_url" value="'.$current_url.'" />';
	echo '</form>';
	echo '</div>';

}
?>

<!-- View Cart Box End -->
</div>
            </div>
        </div>
    </div>
    <!-- /.container -->

<div id="footer">
  <div class="container">
    <p class="text-muted">This Bootstrap Example courtesy <a href="http://www.bootply.com">Bootply.com</a></p>
  </div>
</div>
    <!-- /.container -->
    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>