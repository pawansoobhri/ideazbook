<?php
session_start();

include_once("config.php");

//current URL of the Page. cart_update.php redirects back to this URL
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
$d=$_GET["product_code"];
?>
<!------------------------------------------------------------------------------------------------------------------->
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ShopVol</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/shop-homepage.css" rel="stylesheet">
    <link href="style/style.css" rel="stylesheet" type="text/css">
    


<style type="text/css">

#img{
border-style:1px solid green;
opacity: 0.7;
-webkit-transition: opacity .1s ease-in-out;
-moz-transition: opactiy .1s ease-in-out;
-ms-transition: opacity .1s ease-in-out;
-o-transition: opacity .1s ease-in-out;
transition: opacity .1s ease-in-out;
}

#img:hover{
opacity: 1;
    -webkit-transition: opacity .1s ease-in-out;
    -moz-transition: opactiy .1s ease-in-out;
    -ms-transition: opacity .1s ease-in-out;
    -o-transition: opacity .1s ease-in-out;
    transition: opacity .1s ease-in-out;
}
</style>
<style>
    #dragg
    { 
      position:  absolute;
      left: 0;
      top: 0; /* set these so Chrome doesn't return 'auto' from getComputedStyle */
      width: 200px; 
      background: rgba(255,255,255,0.66); 
      border: 2px  solid rgba(0,0,0,0.5); 
      border-radius: 4px; padding: 8px;
    }
    </style>
    <script>
    function drag_start(event) 
    {
    var style = window.getComputedStyle(event.target, null);
    var str = (parseInt(style.getPropertyValue("left")) - event.clientX) + ',' + (parseInt(style.getPropertyValue("top")) - event.clientY)+ ',' + event.target.id;
    event.dataTransfer.setData("Text",str);
    } 

    function drop(event) 
    {
    var offset = event.dataTransfer.getData("Text").split(',');
    var dm = document.getElementById(offset[2]);
    dm.style.left = (event.clientX + parseInt(offset[0],10)) + 'px';
    dm.style.top = (event.clientY + parseInt(offset[1],10)) + 'px';
    event.preventDefault();
    return false;
    }

    function drag_over(event)
    {
    event.preventDefault();
    return false;
    }   
    </script>
</head>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<body style="overflow:none" ondragover="drag_over(event)" ondrop="drop(event)">
<!-------------------------------------------------------------------------------------------------------------------------------------------->
       <?php include 'nav.html';?>
<!-------------------------------------------------------------------------------------------------------------------------------------------->

<!-- View Cart Box Start -->
<?php
if(isset($_SESSION["cart_products"]) && count($_SESSION["cart_products"])>0)
{
	echo '<div class="cart-view-table-front" id="view-cart" id="view-cart" id="dragg" draggable="true" ondragstart="drag_start(event)">';
	echo '<center><h4>Cart</h4></center>';
	echo '<form method="post" action="cart_update.php">';
	echo '<table width="100%"  cellpadding="6" cellspacing="0">';
	echo '<tbody>';
	$total =0;
	$b = 0;
	foreach ($_SESSION["cart_products"] as $cart_itm)
	{
		$product_name = $cart_itm["product_name"];
		$product_qty = $cart_itm["product_qty"];
		$product_price = $cart_itm["product_price"];
		$product_code = $cart_itm["product_code"];
		$product_color = $cart_itm["product_color"];
		$bg_color = ($b++%2==1) ? 'odd' : 'even'; //zebra stripe
		echo '<tr class="'.$bg_color.'">';
		echo '<td>Qty <input type="text" size="2" maxlength="2" name="product_qty['.$product_code.']" value="'.$product_qty.'" /></td>';
		echo '<td>'.$product_name.'</td>';
		echo '<td><input type="checkbox" name="remove_code[]" value="'.$product_code.'" /> Remove</td>';
		echo '</tr>';
		$subtotal = ($product_price * $product_qty);
		$total = ($total + $subtotal);
	}
	echo '<td colspan="4">';
	echo '<button type="submit" class="btn1 btn1-primary pull-left">Update</button><a class="btn1 btn1-primary pull-right" href="view_cart.php" class="button">Checkout</a>';
	echo '</td>';
	echo '</tbody>';
	echo '</table>';
	
	$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	echo '<input type="hidden" name="return_url" value="'.$current_url.'" />';
	echo '</form>';
	echo '</div>';

}
?>
<!-- View Cart Box End -->
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<?php

$results = $mysqli->query("SELECT id,product_code, product_name,product_det, product_desc,product_edi, product_img_name, price,photo FROM products  WHERE product_code='$d'");
if($results){ 
$products_item = '';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT

<div style="padding:107px 15px 15px; background-color: #fff;  -webkit-box-shadow: 0 0 2px 0 #d3cbb8; -moz-box-shadow: 0 0 2px 0 #d3cbb8; box-shadow: 0 0 2px 0 #d3cbb8; margin: 0 auto; width: 978px;">
<!----------------------------------Product Image---------------------------------------------------------------------------------------------> 
<div style="float:left; width: 300px; min-height: 500px; max-height: 550px;padding-top: 37px;">
<div style="position:relative">
<div id="img" style="flex: 1; display: flex; align-items: center; align-content: center;text-align: center;">
<img src="showimg.php?as=$obj->product_code" style="margin: 0px 42px auto; width:200px; height:250px">           
</div>
</div>
</div>
<!-----------------------------------Product Image ends--------------------------------------------------------------------------------------->
<div style="float: none; _position: relative; _left: -3px; _margin-right: -3px;  width: auto;overflow: hidden;">
<div style="overflow:hidden">
<div style="color: #666; width: 546px;overflow: hidden;">
<div style="border-top: 0; padding-top: 0;border-top: 0px solid #f2f2f2; padding: 10px 0; margin: 0;overflow: hidden;">
<h1>$obj->product_name</h1>
</div>
<!------------------------------------------------------------------------------------------------------------------------------------>
<div style="border-top: 1px solid #f2f2f2;padding: 10px 0;margin: 0;">
<div style="width: 240px; float: left; overflow: hidden;">
<span style="font-family: Museo,Helvetica,arial,san-serif; font-weight: normal; font-size: 28px; color: #565656;">&#8377;&nbsp;{$obj->price}</span>
</div>
<div style="width: auto;overflow: hidden;">
<!---<h3 class="warranty-header">Description</h3>--->
<div style="color: #848484;font-size: 11px;">
<span>
Author: {$obj->product_desc}
<br>
Edition: {$obj->product_edi} 
</span>
</div>
</div>
</div>
</div>
<!------------------------------------------------------------------------------------------------------------------------------------>
<form method="post" action="cart_update.php">
<div style="padding: 10px 0 0 0;border-top: 0;">
<div style=" width: 40%;float: left; ">
<div style="margin-top: 0px;">
<input type="hidden" name="product_code" value="{$obj->product_code}" />
<input type="hidden" name="type" value="add" />
<input type="hidden" name="return_url" value="{$current_url}" />
<button type="submit" class="btn1 btn1-primary pull-left">Add to Cart</button>
</div>
<!---<div style="padding: 0px 20px 4px 109px;"><button type="submit" class="btn1 btn1-primary">Click & Buy</button></div>--->
</div>
<div style="width: 60%; float: left; border-left: 1px solid #d3d3d3; flex-grow: 1; position: relative; padding-bottom: 10px; position: relative; background: #fff;padding-left: 20px;">
<b>Qt.</b>
<input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
</div>
</div>
</form>
<!------------------------------------------------------------------------------------------------------------------------------------>
<!--- <h3 class="warranty-header">Details</h3>--->
<div style="color: #848484;font-size: 11px;padding-top: 60px;">
<span>
<pre style="background-color:ffffff;">{$obj->product_det}</pre>
</span>
</div>
</div>
</div>
</div>


EOT;
}
$products_item .= '';
echo $products_item;
}
?> 
<!---<?php include("./footer.html"); ?>--->
<!-------------------------------------------------------------------------------------------------------------------------------------------->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
</body>
</html>