<?php include 'init_direct.php';?>
<!DOCTYPE html>
<html lang="en">
<?php include("./head.php"); ?>
<body style="overflow:none">
       <?php include 'nav.php';?>
      <div class="container">
        <div class="row" style="padding-top:107px">
           <div class="col-md-6">
<!--Indian Author (Old Book)-->
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10" id="iaob">Indian Author (Old Books)</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT id,product_code, product_name, product_desc, product_img_name, price,photo FROM products  WHERE category LIKE 'n13indold'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products1">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                             <div  class="imgWrap" onClick="location.href='display.php?product_code=$obj->product_code'">
                          <div class="img"><img src="showimg.php?as=$obj->product_code" width="42" height="175"></div>

<div class="box1 imgDescription" style="padding:6px">
                       <div style="white-space: nowrap;text-overflow: ellipsis;" class="product-desc2" >{$obj->product_name}</div>
                       <div style="white-space: nowrap;text-overflow: ellipsis;" class="product-desc1" >{$obj->product_desc}</div>
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
	               </div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
<!--Indian Author (Old Book) Ends-->
<!--Foreign Author (Old Book)-->
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10" id="foob">Foreign Author (Old Books)</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT id, product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE 'n14forold'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products1">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                            <div  class="imgWrap" onClick="location.href='display.php?product_code=$obj->product_code'">
                          <div class="img"><img src="showimg.php?as=$obj->product_code" width="42" height="175"></div>
<div class="box1 imgDescription" style="padding:6px">
                       <div style="white-space: nowrap;text-overflow: ellipsis;" class="product-desc2" >{$obj->product_name}</div>
                       <div style="white-space: nowrap;text-overflow: ellipsis;" class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
	                </div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?> 
<!--Foreign Author (Old Book) End-->
           </div>
<!--Old Book Ends-->
    <div class="col-md-6">
<!--------------------------------------------Indian Author (New Book)---------------------------------------------------------------------------------->
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10" id="ionb">Indian Author (New Books)</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT id, product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE 'n13indnew'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products1">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                            <div  class="imgWrap" onClick="location.href='display.php?product_code=$obj->product_code'">
                          <div class="img"><img src="showimg.php?as=$obj->product_code" width="42" height="175"></div>
<div class="box1 imgDescription" style="padding:6px">
                       <div style="white-space: nowrap;text-overflow: ellipsis;" class="product-desc2" >{$obj->product_name}</div>
                       <div style="white-space: nowrap;text-overflow: ellipsis;" class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
	               </div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>
<!--------------------------------------------Foreign Author (New Book)---------------------------------------------------------------------------------->
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10" id="fonb">Foreign Author (New Books)</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT id, product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE 'n14fornew'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products1">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                             <div  class="imgWrap" onClick="location.href='display.php?product_code=$obj->product_code'">
                          <div class="img"><img src="showimg.php?as=$obj->product_code" width="42" height="175"></div>

<div class="box1 imgDescription" style="padding:6px">
                       <div style="white-space: nowrap;text-overflow: ellipsis;" class="product-desc2" >{$obj->product_name}</div>
                       <div style="white-space: nowrap;text-overflow: ellipsis;" class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
	               </div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>     
</div>
        </div>  
    </div>
<?php include("./footer.html"); ?>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>