<?php include 'init_direct.php';?>
<!DOCTYPE html>
<html lang="en">
   <?php include("./head.php"); ?>
   <body style="overflow:none">
      <div><?php include 'nav.php';?></div>
      <div class="container">
         <div class="row" style="padding-top:107px">
            <div class="col-md-9">
               <?php include("./sliders.html"); ?>           
               <?php include("./products.php"); ?> 
               <hr>
            </div>
            <div class="col-md-3">
<?php
$results = $mysqli->query("SELECT id,product_code, product_name, product_desc, product_img_name, price,photo FROM products  WHERE category LIKE '%indold' OR category LIKE '%forold' OR category LIKE '%indnew' OR category LIKE '%fornew'   ORDER BY RAND() LIMIT 24");
if($results){ 
$products_item = '<ul class="products1">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
<li class="product">
<form method="post" action="cart_update.php">
<div class="thumbnail2  box">
<div class="product-content">
<div class="product-thumb" >
<div  class="imgWrap" onClick="location.href='display.php?product_code=$obj->product_code'">
<div class="img"><img src="showimg.php?as=$obj->product_code" width="42" height="175"></div>
<div class="box1 imgDescription" style="background: linear-gradient(to bottom,#624A4A 0,rgba(255, 93, 0, 0.18) 100%); padding:6px" >
<div style=" white-space: nowrap;text-overflow: ellipsis;" class="product-desc2" >{$obj->product_name}</div>
<div style=" white-space: nowrap;text-overflow: ellipsis;" class="product-desc1" >{$obj->product_desc}</div>
<span>{$currency}{$obj->price}
Qt.
<input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
<input type="hidden" name="product_code" value="{$obj->product_code}" />
<input type="hidden" name="type" value="add" />
<input type="hidden" name="return_url" value="{$current_url}" />
<p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div></div></div></div></div></form></li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
  </div>
         </div>
      </div>
     
      <?php include("./footer.html"); ?>
      <script src="./js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      
   </body>
</html>