<!DOCTYPE html>
<?php include 'init_direct.php';?>
<?php include 'head.php';?>
<html>
<head>
<link href="css/popup_modal.css" rel="stylesheet" type="text/css">
</head>
<body>

<div id="myModal" class="modal">
  <!-- Modal content -->
  <span class="close">&times;</span>
  <div class="modal-content bgimage">    
    <div class="row modal-body">
        <div  style="padding: 15px;">
      <h4>Register</h4>
                  <?php
                  if(isset($_SESSION["showmydiv"])){
                      if($_SESSION['showmydiv']=='block'){
                        echo '<div style="display:none;display:'.$_SESSION['showmydiv'].';margin-bottom: 25px;"><span style="color: #ff5d00;">User Already Exists</span></div>';
                      }
                  }
                  unset($_SESSION['showmydiv']);
                    ?>
                     <form action="adduser.php" accept="image/gif, image/jpeg" method="post" enctype="multipart/form-data">
                        <table style="width: -webkit-fill-available;">
                           <tr>
                              <td>First Name</td>
                              <td>
                                 <input type="text" placeholder="Jack" class="form-control" name="fname" required/>
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Last Name</td>
                              <td>
                                 <input type="text" placeholder="Sparrow" class="form-control" name="lname" required />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Address</td>
                              <td>
                                 <input type="text" placeholder="123, Streen No:" class="form-control" name="address" required/>
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>City</td>
                              <td>
                                 <input type="text" placeholder="Mumbai" class="form-control" name="city" required />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Pin Code</td>
                              <td>
                                 <input type="number" placeholder="144602" class="form-control" name="pin" required/>
                                 <p></p>
                              </td>
                           </tr>                          
                           <tr>
                              <td>E-Mail </td>
                              <td>
                                 <input class="form-control" type="email" placeholder="apc@xyz.com;" name="email" required/>
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Password </td>
                              <td>
                                 <input class="form-control" type="password" name="pwd" required />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td colspan="2"><input type="submit" value="Register" style="width: -webkit-fill-available;"class="btn1 btn1-primary pull-left btn-lg" /></td>
                           </tr>
                        </table>
                     </form>
                     <div>
                       <td colspan="2"><input type="submit" onclick="location.href='./login.php';" value="Already have an account? Login" style="width: -webkit-fill-available;"class="btn1 btn1-primary pull-left btn-lg" /></td>
                     </div>
      </div>
     </div>
  </div>
</div>
<script src="js/popup_modal.js"></script>

</body>
</html>
