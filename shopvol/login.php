<?php if(session_id() == '' || !isset($_SESSION)){
    session_start();
    }
    
    ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Login</title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/shop-homepage.css" rel="stylesheet">
      <link href="style/style.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="http://www.abhiyatra.com/css/datepicker.css">
      <link rel="shortcut icon" type="image/png" href="images/favi.png">
      <script src="http://www.abhiyatra.com/js/bootstrap-datepicker.js"></script>
   </head>
   <body style="overflow:none">
      <div id="wrapper">
         <div id="header">
            <?php include 'nav.php';?>
         </div>
         
         <div id="content">
            <div class="container">
               <div class="row" style="margin:150px 0px">
                  <div class="col-md-9">
                  <?php
                  if(isset($_SESSION["showmydiv"])){
                      if($_SESSION['showmydiv']=='block'){
                        echo '<div style="display:none;display:'.$_SESSION['showmydiv'].';margin-bottom: 25px;"><span style="color: #ff5d00;">User Already Exists</span></div>';
                      }
                      elseif($_SESSION['showmydiv']=='invalid_user'){
                        echo '<div style="display:'.$_SESSION['showmydiv'].';margin-bottom: 25px;"><span style="color: #ff5d00;">Invalid UserId or Password</span></div>';
                      }
                      else{
                          echo '<div style="margin-bottom: 25px;"><span style="color: #549e01;">You are Registered</span></div>';
                      }
                  }
                  unset($_SESSION['showmydiv']);
                    ?>
                     <form action="verify.php" accept="image/gif, image/jpeg" method="post" enctype="multipart/form-data">
                        <table>                          
                           <tr>
                              <td>E-Mail&nbsp;&nbsp;&nbsp; </td>
                              <td>
                                 <input class="form-control" type="email" placeholder="apc@xyz.com;" name="username" />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Password&nbsp;&nbsp;&nbsp; </td>
                              <td>
                                 <input class="form-control" type="password" name="pwd" />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td><input type="submit" value="Login" class="btn1 btn1-primary pull-right" /></td>
                              <td><input type="reset"  value="Reset" class="btn1 btn1-primary pull-right"></td>
                           </tr>
                        </table>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div id="footer">   
            <?php include("./footer.html"); ?>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
   </body>
</html>