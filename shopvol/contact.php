<?php
session_start();

include_once("config.php");

//current URL of the Page. cart_update.php redirects back to this URL
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ShopVol | The largest online book store</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/pawan.css" rel="stylesheet">
    <link href="css/shop-homepage.css" rel="stylesheet">
    <link href="style/style.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/png" href="images/favi.png">
<style>
    #dragg
    { 
      position:  absolute;
      left: 0;
      top: 0; /* set these so Chrome doesn't return 'auto' from getComputedStyle */
      width: 200px; 
      background: rgba(255,255,255,0.66); 
      border: 2px  solid rgba(0,0,0,0.5); 
      border-radius: 4px; padding: 8px;
    }
@import url(http://fonts.googleapis.com/css?family=Montserrat:400,700);

html{    background:url(http://thekitemap.com/images/feedback-img.jpg) no-repeat;
  background-size: cover;
  height:100%;
}

#feedback-page{
	text-align:center;
}

#form-main{
	width:100%;
	float:left;
	padding-top:0px;
}

#form-div {
	background-color:rgba(72,72,72,0.4);
	padding-left:35px;
	padding-right:35px;
	padding-top:35px;
	padding-bottom:50px;
	width: 450px;
	float: left;
	left: 50%;
	position: absolute;
  margin-top:30px;
	margin-left: -260px;
  -moz-border-radius: 7px;
  -webkit-border-radius: 7px;
}

.feedback-input {
	color:#3c3c3c;
	font-family: Helvetica, Arial, sans-serif;
  font-weight:500;
	font-size: 18px;
	border-radius: 0;
	line-height: 22px;
	background-color: #fbfbfb;
	padding: 13px 13px 13px 54px;
	margin-bottom: 10px;
	width:100%;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-ms-box-sizing: border-box;
	box-sizing: border-box;
  border: 3px solid rgba(0,0,0,0);
}

.feedback-input:focus{
	background: #fff;
	box-shadow: 0;
	border: 3px solid #3498db;
	color: #3498db;
	outline: none;
  padding: 13px 13px 13px 54px;
}

.focused{
	color:#30aed6;
	border:#30aed6 solid 3px;
}

/* Icons ---------------------------------- */
#name{
	background-image: url(http://rexkirby.com/kirbyandson/images/name.svg);
	background-size: 30px 30px;
	background-position: 11px 8px;
padding-left: 47px;
	background-repeat: no-repeat;
}

#name:focus{
	background-image: url(http://rexkirby.com/kirbyandson/images/name.svg);
	background-size: 30px 30px;
	background-position: 8px 5px;
  background-position: 11px 8px;
	background-repeat: no-repeat;
}

#email{
	background-image: url(http://rexkirby.com/kirbyandson/images/email.svg);
	background-size: 30px 30px;
	background-position: 11px 8px;
padding-left: 47px;
	background-repeat: no-repeat;
}

#email:focus{
	background-image: url(http://rexkirby.com/kirbyandson/images/email.svg);
	background-size: 30px 30px;
  background-position: 11px 8px;
	background-repeat: no-repeat;
}

#comment{
	background-image: url(http://rexkirby.com/kirbyandson/images/comment.svg);
	background-size: 30px 30px;
	background-position: 11px 8px;
	background-repeat: no-repeat;
}

textarea {
    width: 100%;
    height: 150px;
    line-height: 150%;
    resize:vertical;
}

input:hover, textarea:hover,
input:focus, textarea:focus {
	background-color:white;
}

#button-blue{
	font-family: 'Montserrat', Arial, Helvetica, sans-serif;
	float:left;
	width: 100%;
	border: #fbfbfb solid 4px;
	cursor:pointer;
	background-color: #3498db;
	color:white;
	font-size:24px;
	padding-top:22px;
	padding-bottom:22px;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
  margin-top:-4px;
  font-weight:700;
}

#button-blue:hover{
	background-color: rgba(0,0,0,0);
	color: #0493bd;
}
	
.submit:hover {
	color: #3498db;
}
	
.ease {
	width: 0px;
	height: 74px;
	background-color: #fbfbfb;
	-webkit-transition: .3s ease;
	-moz-transition: .3s ease;
	-o-transition: .3s ease;
	-ms-transition: .3s ease;
	transition: .3s ease;
}

.submit:hover .ease{
  width:100%;
  background-color:white;
}

@media only screen and (max-width: 580px) {
	#form-div{
		left: 3%;
		margin-right: 3%;
		width: 88%;
		margin-left: 0;
		padding-left: 3%;
		padding-right: 3%;
	}
}


    </style>
    <script>
    function drag_start(event) 
    {
    var style = window.getComputedStyle(event.target, null);
    var str = (parseInt(style.getPropertyValue("left")) - event.clientX) + ',' + (parseInt(style.getPropertyValue("top")) - event.clientY)+ ',' + event.target.id;
    event.dataTransfer.setData("Text",str);
    } 

    function drop(event) 
    {
    var offset = event.dataTransfer.getData("Text").split(',');
    var dm = document.getElementById(offset[2]);
    dm.style.left = (event.clientX + parseInt(offset[0],10)) + 'px';
    dm.style.top = (event.clientY + parseInt(offset[1],10)) + 'px';
    event.preventDefault();
    return false;
    }

    function drag_over(event)
    {
    event.preventDefault();
    return false;
    }   
    </script>
</head>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<body style="overflow:none;">
<!-------------------------------------------------------------------------------------------------------------------------------------------->
       <div><?php include 'nav.php';?></div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
      <div class="container">
        <div class="row" style="padding-top:107px">
           <div class="col-md-12">
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------------------------------------------------------->
              <div class="section-header" style="text-align: center; padding-bottom: 10px;">
<h2 class="white-text" style="padding-bottom: 10px;line-height: 40px;position: relative;display: inline-block;font-size: 45px;text-transform: uppercase;margin-top: 15px;margin-bottom: 0;color: #ffffff;">Contact Us</h2><div class="white-text section-legend">We are there to answer.</div></div>
<section>
<form  name="contactForm" id='contact_form' method="post" action='php/email.php'> 
<input type="hidden" name="action" value="submit"> 

<div class="col-lg-4 col-sm-4"  data-sr-init="true" data-sr-complete="true">
<label for="name" style="clip: rect(1px, 1px, 1px, 1px); position: absolute !important;">Your Name</label>
<input required="required" type="text" name="name" id="name" placeholder="Your Name" class="form-control input-box" style="width: 100%; margin: auto; margin-bottom: 20px; border-radius: 4px;height: 50px;" value=""></div> 


<div class="col-lg-4 col-sm-4"  data-sr-init="true" data-sr-complete="true">
<label for="email" class="screen-reader-text" style="clip: rect(1px, 1px, 1px, 1px); position: absolute !important;">Your Email</label>
<input required="required" type="email" name="email" id="email" placeholder="Your Email" class="form-control input-box" value="" style="width: 100%; margin: auto; margin-bottom: 20px; border-radius: 4px;height: 50px;"></div>


<div class="col-lg-4 col-sm-4"  data-sr-init="true" data-sr-complete="true">
<label for="subject" class="screen-reader-text" style="clip: rect(1px, 1px, 1px, 1px); position: absolute !important;">Subject</label>
<input required="required" type="text" name="subject" id="subject" placeholder="Subject" class="form-control input-box" value="" style="width: 100%; margin: auto; margin-bottom: 20px; border-radius: 4px;    height: 50px;"></div>

<div class="col-lg-12 col-sm-12" data-scrollreveal="enter right after 0s over 1s" data-sr-init="true" data-sr-complete="true">
<label for="message" class="screen-reader-text" class="screen-reader-text" style="clip: rect(1px, 1px, 1px, 1px); position: absolute !important;">Your Message</label>
<textarea name="message" id="message" class="form-control textarea-box" placeholder="Your Message" style="text-align: left; text-transform: none; padding: 9px; min-height: 250px; padding-left: 15px; display: inline-block; border-radius: 4px; background: rgba(255,255,255, 0.95);"></textarea>
</div>

<button class="btn btn-primary custom-button red-btn" type="submit" data-sr-init="true" data-sr-complete="true" style="float: right; margin-right: 15px; color: #FFF !important; -webkit-transition: all 0.3s ease-in-out; transition: all 0.3s ease-in-out; background: #549E01; display: inline-block !important; text-align: center; text-transform: uppercase; padding: 13px 35px 13px 35px; border-radius: 4px; margin: 10px; border: none;" id='send_message' value="send" name="send">Submit<div class="g-recaptcha" value="Send email"/></div></button>
</form> 
</section>
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
        </div>  
    </div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<?php include("./footer.html"); ?>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
</body>
</html>