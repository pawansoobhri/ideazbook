<?php
session_start();
include_once("config.php");
?>

<!DOCTYPE html>
<html lang="en">
<?php include("./head.php"); ?>
<body style="overflow:none;" ondragover="drag_over(event)" ondrop="drop(event)">

     <div>  <?php include 'nav.php';?></div>

<!-- View Cart Box Start -->
<?php
//current URL of the Page. cart_update.php redirects back to this URL
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
$ss=$_GET["product_name"];
$e="NULL";
if(isset($_SESSION["cart_products"]) && count($_SESSION["cart_products"])>0)
{
	echo '<div class="cart-view-table-front" id="view-cart" id="view-cart" id="dragg" draggable="true" ondragstart="drag_start(event)">';
	echo '<center><h4>Cart</h4></center>';
	echo '<form method="post" action="cart_update.php">';
	echo '<table width="100%"  cellpadding="6" cellspacing="0">';
	echo '<tbody>';

	$total =0;
	$b = 0;
	foreach ($_SESSION["cart_products"] as $cart_itm)
	{
		$product_name = $cart_itm["product_name"];
		$product_qty = $cart_itm["product_qty"];
		$product_price = $cart_itm["product_price"];
		$product_code = $cart_itm["product_code"];
		//$product_color = $cart_itm["product_color"];
		$bg_color = ($b++%2==1) ? 'odd' : 'even'; //zebra stripe
		echo '<tr class="'.$bg_color.'">';
		echo '<td>Qty <input type="text" size="2" maxlength="2" name="product_qty['.$product_code.']" value="'.$product_qty.'" /></td>';
		echo '<td>'.$product_name.'</td>';
		echo '<td><input type="checkbox" name="remove_code[]" value="'.$product_code.'" /> Remove</td>';
		echo '</tr>';
		$subtotal = ($product_price * $product_qty);
		$total = ($total + $subtotal);
	}
	echo '<td colspan="4">';
	echo '<button type="submit" class="btn1 btn1-primary pull-left">Update</button><a class="btn1 btn1-primary pull-right" href="view_cart.php" class="button">Checkout</a>';
	echo '</td>';
	echo '</tbody>';
	echo '</table>';
	
	$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	echo '<input type="hidden" name="return_url" value="'.$current_url.'" />';
	echo '</form>';
	echo '</div>';

}
?>
<!-- View Cart Box End -->
<div class="container1" style="margin-top:140px">

<?php
$results = $mysqli->query("SELECT id,product_code, product_name,product_det,category, product_desc,product_edi, product_img_name, price,photo,mrp FROM products  WHERE product_name like '%".$ss."%'");
$ass=1;
if($results){ 
$products_item = '';

//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$ass=2;
$e=$obj->category;
$a=substr($e,6);
if($a=="old")
$a="Old";
$products_item .= <<<EOT
<div class="container" >
<div class="jumbotron1" style="background-color: rgba(255, 255, 255, 0.88);  -webkit-box-shadow: 0 0 2px 0 #d3cbb8; -moz-box-shadow: 0 0 2px 0 #d3cbb8; box-shadow: 0 0 2px 0 #d3cbb8; margin: 0 auto;">
<div class="row" style="padding-bottom:19px">
<div class="col-sm-6 col-md-4 col-lg-4">
<img src="showimg.php?as=$obj->product_code" style="margin: 19px 49px auto; width:131px; height:158px">           
</div>
<div class="col-sm-6 col-md-4 col-lg-8">
<u><h3 onClick="location.href='display.php?product_code=$obj->product_code'">$obj->product_name</h3></u>
<div style="border-top: 1px solid #f2f2f2;padding: 0px 0;margin-bottom: 10px;">
<div style="width: 151px; float: left; overflow: hidden;     margin-top: 10px;">
<span style="font-family: Museo,Helvetica,arial,san-serif; font-weight: normal; font-size: 18px; color: #565656;">&#8377;&nbsp;{$obj->price}</span>
<br>
<strike><span style="font-family: Museo,Helvetica,arial,san-serif; font-weight: normal;color: #565656;">&#8377;&nbsp;{$obj->mrp}</span></strike>
</div>
</div>
<div style="color: #848484;font-size: 9px;">
<span>
Author: {$obj->product_desc}
<br>
Edition: {$obj->product_edi} 
<br>
Status: {$a} 
</span>
</div>
<div class="clearfix visible-sm-block"></div>
<form method="post" action="cart_update.php">
<div style="padding: 15px 0 0 0;border-top: 0;">
<div style=" float: left;margin-left: -6px; ">
<div style="margin-top: 7px;">
<input type="hidden" name="product_code" value="{$obj->product_code}" />
<input type="hidden" name="type" value="add" />
<input type="hidden" name="return_url" value="{$current_url}" />
<button type="submit" class="btn1 btn1-primary pull-left">Add to Cart</button>
</div>
<!---<div style="padding: 0px 20px 4px 109px;"><button type="submit" class="btn1 btn1-primary">Click & Buy</button></div>--->
</div>
<div style=" float: left; /*border-left: 1px solid #d3d3d3;*/ flex-grow: 1; position: relative; padding-top: 7px; position: relative; padding-left: 0px;margin-left: 47px;">
<b>Qt.</b>
<input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

EOT;
}


$products_item .= '';
if($ass==1)
{
$products_item = '';
$products_item .= <<<EOT
<div class="col-md-2"></div>
<div class="col-md-7">
<font color="#fff" size="2.1em">If your search didn't match, do fill the details of your books.</br>We will display your book in 60-120 minutes</br></br></font>
<form action="nosearch.php" accept="image/gif, image/jpeg" method="post" enctype="multipart/form-data">
<table>
<tr><td><font color="#fff">Name</font>&nbsp;&nbsp;&nbsp;</td><td><input type="text" placeholder="eg. Digital Electronics" class="form-control" name="product_name" /><p></p></td></tr>

<tr><td><font color="#fff">Author</font>&nbsp;&nbsp;&nbsp;</td><td><input type="text" placeholder="eg. Morris Mano" class="form-control" name="product_author" /><p></p></td></tr>

<tr><td><font color="#fff">Edition</font>&nbsp;&nbsp;&nbsp;</td><td><input type="text" placeholder="eg. 2" class="form-control" name="product_edition" /><p></p></td></tr>

<tr><td><font color="#fff">Condition</font>&nbsp;&nbsp;&nbsp;</td><td><select name="category1" class="form-control">
  <option value="indnew">Indian Author New</option>
  <option value="indold">Indian Author Old</option>
  <option value="fornew">Foreign Author New</option>
  <option value="forold">Foreign Author Old</option>
</select><p></p></td></tr>


<tr><td></td><td><input type="submit" class="btn1 btn1-primary pull-right" /></td></tr></table>
</form>
EOT;
echo $products_item;
}
else

echo $products_item;
}

?> 
</div>
<div class="col-md-3"></div>
</div>



</body>
<?php include("./footer.html"); ?>
 <script src="./js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
</body>
</html>