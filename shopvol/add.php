<!DOCTYPE html>
<?php
   session_start();
   include_once("config.php");
   //current URL of the Page. cart_update.php redirects back to this URL
   $current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
   ?>
<!DOCTYPE html>
<html lang="en">
   <?php include("./head.php"); ?>
   <body style="overflow:none">
      <?php include 'nav.php';?>
      <div class="container">
         <div class="row" style="padding-top:107px">
            <div class="col-md-9">
               <?php
                  include_once("config.php");
                  
                  $product_name=$_POST["product_name"];
                  $product_author=$_POST["product_author"];
                  $product_edition=$_POST["product_edition"];
                  //$product_img=$_POST["product_img_name"];
                  $category=$_POST["category"];
                  $category1=$_POST["category1"];
                  $price=$_POST["price"];
                  $mrp=$_POST["mrp"];
                  $temp=$product_author."  ".$product_edition;
                  $temp1=$category.$category1;
                  $product_code="SV".date('Ymd-His');
                  $imagename=$_FILES["myimage"]["name"]; 
                  $product_det=$_POST["product_det"];
                  
                  //Get the content of the image and then add slashes to it 
                  $imagetmp=addslashes (file_get_contents($_FILES['myimage']['tmp_name']));
                  
                  //$y= $mysqli->query("insert into products (product_code,product_name,product_img_name,category,price,product_desc,photo,product_det,product_edi,mrp) value('$product_code','$product_name','$product_img','$temp1','$price','$product_author','$imagetmp','$product_det','$product_edition','$mrp')");
                  $y= $mysqli->query("insert into products (product_code,product_name,category,price,product_desc,photo,product_det,product_edi,mrp) value('$product_code','$product_name','$temp1','$price','$product_author','$imagetmp','$product_det','$product_edition','$mrp')");
                  if ($y==1)
                  echo "<h1>Your details have been successfully uploaded</h1><br><h3>Add another product&nbsp;&nbsp;<a href='./insert.php' style='color:red'>Add your own Books</a></h3>";
                  else
                  echo "<h1>Sorry! The details were not correct</h1><br><h3>Make it again&nbsp;&nbsp;<a href='./insert.php' style='color:red'>Add your own Books</a></h3>";
                  ?>
            </div>
         </div>
      </div>
      <?php include("./footer.html"); ?>
      <script src="js/jquery.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="js/bootstrap.min.js"></script>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
   </body>
</html>