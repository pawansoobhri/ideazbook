<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Update</title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/shop-homepage.css" rel="stylesheet">
      <link href="style/style.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="http://www.abhiyatra.com/css/datepicker.css">
      <link rel="shortcut icon" type="image/png" href="images/favi.png">
      <script src="http://www.abhiyatra.com/js/bootstrap-datepicker.js"></script>
   </head>
   <body style="overflow:none">
      <div id="wrapper">
         <div id="header">
            <?php include 'nav.php';?>
         </div>
         <div id="content">
            <div class="container">
               <div class="row" style="margin:150px 0px">
                  <div class="col-md-9">
                     <span style="font-size: 1.65em;">Your Profile has been Updated!!!</span>
                  </div>
               </div>
            </div>
         </div>
         
         <div id="footer">   
            <?php include("./footer.html"); ?>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
   </body>
</html>