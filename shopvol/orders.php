<?php
  if(session_id() == '' || !isset($_SESSION)){session_start();}
  include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">
<?php include("./head.php"); ?>
   <body style="overflow:none">
      <div id="wrapper">
      <div id="header">
         <?php include 'nav.php';?>
      </div>
      <div id="content">
         <div class="container">
            <div class="row" style="margin:107px 0px">
              
        <h3>My COD Orders</h3>
        
        <hr>

        <?php
          $user = $_SESSION["username"];
          $result = $mysqli->query("SELECT * from orders where email='".$user."'");
          if($result) {
            while($obj = $result->fetch_object()) {
              //echo '<div class="large-6">';
              echo '<div style="float:left;margin-right: 30px;">';
              echo '<p><h4>Order ID:'.$obj->id.'</h4></p>';
              echo '<p><strong>Date of Purchase</strong>: '.$obj->date.'</p>';
              echo '<p><strong>Product Code</strong>: '.$obj->product_code.'</p>';
              echo '<p><strong>Product Name</strong>: '.$obj->product_name.'</p>';
              echo '<p><strong>Price Per Unit</strong>: '.$obj->price.'</p>';
              echo '<p><strong>Units Bought</strong>: '.$obj->qty.'</p>';
              echo '<p><strong>Total Cost</strong>: '.$currency.$obj->subtotal.'</p>';
              //echo '</div>';
              //echo '<div class="large-6">';
              //echo '<img src="images/products/sports_band.jpg">';
              //echo '</div>';
              echo '<p><hr></p>';
              echo '</div>';

            }
          }
          
        ?>
      
         <div id="footer">   
            <?php include("./footer.html"); ?>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
   </body>
</html>