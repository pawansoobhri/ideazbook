<!DOCTYPE html>
<html lang="en">
<?php include("./head.php"); ?>
   <body style="overflow:none">
      <div id="wrapper">
         <div id="header">
            <?php include 'nav.php';?>
         </div>
         <div id="content">
            <div class="container">
               <div class="row" style="padding-top:107px">
                  <div class="col-md-9">
                     <form action="add.php" accept="image/gif, image/jpeg" method="post" enctype="multipart/form-data">
                        <table>
                           <tr>
                              <td>Name&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <input type="text" placeholder="eg. Digital Electronics" class="form-control" name="product_name" />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Author&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <input type="text" placeholder="eg. Morris Mano" class="form-control" name="product_author" />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Detail&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <input type="text" placeholder="eg. Morris Mano" class="form-control" name="product_det" />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Edition&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <input type="text" placeholder="eg. 2" class="form-control" name="product_edition" />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>MRP&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <input type="text" placeholder="eg. 2" class="form-control" name="mrp" />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Image&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <!--<input type="text" placeholder="eg. 2" class="form-control" name="product_img" />-->
                                 <input class="form-control" type="file" name="myimage">
                                 <p></p>
                              </td>
                           </tr>
                           <!--<tr><td>Date&nbsp;&nbsp;&nbsp;</td><td> <input placeholder="Date" class="form-control" onfocus="(this.type='date')" onblur="(this.type='text')" id="date" type="text">
                              <p></p></td></tr>-->
                           <tr>
                              <td>Category&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <select name="category" class="form-control">
                                    <option value="n01">NCERT I</option>
                                    <option value="n02">NCERT II</option>
                                    <option value="n03">NCERT III</option>
                                    <option value="n04">NCERT IV</option>
                                    <option value="n05">NCERT V</option>
                                    <option value="n06">NCERT VI</option>
                                    <option value="n07">NCERT VII</option>
                                    <option value="n08">NCERT VIII</option>
                                    <option value="n09">NCERT IX</option>
                                    <option value="n10">NCERT X</option>
                                    <option value="n11">NCERT XI</option>
                                    <option value="n12">NCERT XII</option>
                                    <option value="n13">College-Indian</option>
                                    <option value="n14">College-Foregin</option>
                                    <option value="n15">Competition Success</option>
                                 </select>
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Condition&nbsp;&nbsp;&nbsp;</td>
                              <td>
                                 <select name="category1" class="form-control">
                                    <option value="indnew">Indian Author New</option>
                                    <option value="indold">Indian Author Old</option>
                                    <option value="fornew">Foreign Author New</option>
                                    <option value="forold">Foreign Author Old</option>
                                 </select>
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td>Price&nbsp;&nbsp;&nbsp; </td>
                              <td>
                                 <input class="form-control" type="text" placeholder="&#8377;" name="price" />
                                 <p></p>
                              </td>
                           </tr>
                           <tr>
                              <td></td>
                              <td><input type="submit" class="btn1 btn1-primary pull-right" /></td>
                           </tr>
                        </table>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div id="footer">   
            <?php include("./footer.html"); ?>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
   </body>
</html>