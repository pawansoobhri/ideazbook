<?php
session_start();
include_once("config.php");

//current URL of the Page. cart_update.php redirects back to this URL
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ShopVol | The largest online book store</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/shop-homepage.css" rel="stylesheet">
    <link href="style/style.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/png" href="images/favi.png">
</head>

<body style="overflow:none">

       <?php include 'nav.php';?>

      <div class="container">
        <div class="row" style="padding-top:107px">
           <div class="col-md-12">
       <div class="hp-section-header">
         <h2 class="title">
            <span class="fllt fk-uppercase fk-font-16 lmargin10" id="I">Class I</span>
            <span class="fk-font-13 flrt rmargin10 subText"></span>
         </h2>
       </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n1'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col-md-12">
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10">Class II</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n2'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col-md-12">
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10">Class III</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n3'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col-md-12">
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10">Class IV</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n4'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col-md-12">
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10" id="V">Class V</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n5'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col-md-12">
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10" id="VI">Class VI</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n6'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col-md-12">
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10">Class VII</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n7'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col-md-12">
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10" id="VIII">Class VIII</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n8'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col-md-12">
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10" id="IX">Class IX</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n9'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col-md-12">
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10" id="X">Class X</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n10'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col-md-12">
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10" id="XI">Class XI</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n11'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col-md-12">
              <div class="hp-section-header">
                <h2 class="title">
                    <span class="fllt fk-uppercase fk-font-16 lmargin10">Class XII</span>
                    <span class="fk-font-13 flrt rmargin10 subText"></span>
                </h2>
              </div>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products  WHERE category LIKE '%n12'  ORDER BY id DESC");
if($results){ 
$products_item = '<ul class="products2">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product">
	<form method="post" action="cart_update.php">
        <div class="thumbnail2  box">
	<div class="product-content">
	               <div class="product-thumb">
                           <div class="imgWrap">
                            <img src="images/{$obj->product_img_name}" width="42" height="175"> 

<div class="box1 imgDescription" style="padding:6px">
                       <div class="product-desc2" >{$obj->product_name}</div>
                       <div class="product-desc1" >{$obj->product_desc}</div>
	
	               <span>{$currency}{$obj->price}
	               Qt.
	               <input type="text" size="2" maxlength="2" name="product_qty" value="1" /></span>
	               <input type="hidden" name="product_code" value="{$obj->product_code}" />
	               <input type="hidden" name="type" value="add" />
	               <input type="hidden" name="return_url" value="{$current_url}" />
                       <p style="margin: 21px 0 10px;"><button type="submit" class="btn1 btn1-primary pull-left">Add</button></p>
</div> </div> </div> </div> </div>
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;
}
?>    
</div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
        </div>  
    </div>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
<?php include("./footer.html"); ?>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<!-------------------------------------------------------------------------------------------------------------------------------------------->
</body>
</html>